<?php

class M_Profil extends CI_Model
{

	function cek_profil($type,$data)
	{
		$result = $this->db->query("select * from pembeli WHERE $type='$data' limit 1");
		return $result;
	}

	function cek_password($id_pembeli,$password)
	{
		$result = $this->db->query("select * from pembeli WHERE id_pembeli='$id_pembeli' and password=md5('$password') limit 1");
		return $result;
	}

    function tampil_profil($id_pembeli)
	{
		$result = $this->db->query("select * from pembeli WHERE id_pembeli='$id_pembeli' LIMIT 1");
		return $result;
	}

	function tampil_alamat($id_pembeli)
	{
		$result = $this->db->query("select * from alamat
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			WHERE alamat.id_pembeli='$id_pembeli'");
		return $result;
	}

	function ubah_profil($id_pembeli,$nama,$nomor_telepon,$email)
	{
		$result = $this->db->query("update pembeli set nama='$nama',nomor_telepon='$nomor_telepon',email='$email' where id_pembeli='$id_pembeli'");
		return $result;
	}

	function tambah_alamat($nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan)
	{
		if($default=='1'){
			$result = $this->db->query("update alamat set default='0' where id_pembeli='$id_pembeli'");
		}
		$result = $this->db->query("insert into alamat values ('null','$nama_penerima','$nomor_telepon','$alamat','$default','$id_pembeli','$id_kecamatan')");
		return $result;
	}

	function ubah_alamat($id_alamat,$nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan)
	{
		if($default=='1'){
			$result = $this->db->query("update alamat set `default`='0' where id_pembeli='$id_pembeli'");
		}
		$result = $this->db->query("update alamat set nama_penerima='$nama_penerima',nomor_telepon='$nomor_telepon',alamat='$alamat',`default`='$default',id_kecamatan='$id_kecamatan' where id_alamat='$id_alamat'");
		return $result;
	}

	function ubah_password($id_pembeli,$password)
	{
		$result = $this->db->query("update pembeli set password=md5('$password') where id_pembeli='$id_pembeli'");
		return $result;
	}

	function ubah_status($id_pesanan,$status)
	{
		$result = $this->db->query("update pesanan set status_pesanan='$status',tanggal_diterima=now() where id_pesanan='$id_pesanan'");
		return $result;
	}
}											
