<?php

class M_Jenis_Barang extends CI_Model
{
	
	function tampil()
	{
		$result = $this->db->query("select * from jenis_barang order by jenis_barang asc");
		return $result;
    }
    
    function cek_nama($nama)
	{
		$result = $this->db->query("select * from jenis_barang WHERE jenis_barang='$nama' LIMIT 1");
		return $result;
	}
    
    function tampil_jenis_barang($id_jenis_barang)
	{
		$result = $this->db->query("select * from jenis_barang WHERE id_jenis_barang='$id_jenis_barang' LIMIT 1");
		return $result;
	}
    
    function barang_jenis_barang($id_jenis_barang)
	{
		$result = $this->db->query("select * from barang WHERE id_jenis_barang='$id_jenis_barang' LIMIT 1");
		return $result;
	}

	function simpan($jenis_barang)
	{
		$result = $this->db->query("insert into jenis_barang values ('null','$jenis_barang')");
		return $result;
	}

	function ubah($id_jenis_barang,$jenis_barang)
	{
		$result = $this->db->query("update jenis_barang set jenis_barang='$jenis_barang' where id_jenis_barang='$id_jenis_barang'");
		return $result;
	}

	function hapus($id_jenis_barang)
	{
		$result = $this->db->query("delete from jenis_barang where id_jenis_barang='$id_jenis_barang'");
		return $result;
	}
}											
