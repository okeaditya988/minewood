<?php

class M_Barang extends CI_Model
{
	
	function tampil()
	{
		$result = $this->db->query("select * from barang
			inner join jenis_barang on jenis_barang.id_jenis_barang=barang.id_jenis_barang
			order by jenis_barang.jenis_barang asc,barang.nama_barang asc");
		return $result;
	}
    
    function cek_nama($nama,$id_jenis_barang)
	{
		$result = $this->db->query("select * from barang WHERE nama_barang='$nama' and id_jenis_barang='$id_jenis_barang' LIMIT 1");
		return $result;
	}
    
    function tampil_barang($id_barang)
	{
		$result = $this->db->query("select * from barang 
			inner join jenis_barang on jenis_barang.id_jenis_barang=barang.id_jenis_barang
			WHERE id_barang='$id_barang' LIMIT 1");
		return $result;
	}
    
    function detail_pesanan_barang($id_barang)
	{
		$result = $this->db->query("select * from detail_pesanan WHERE id_barang='$id_barang' LIMIT 1");
		return $result;
	}

	function simpan($nama_barang,$foto_barang,$stok,$berat,$harga_beli,$harga_jual,$deskripsi,$id_jenis_barang)
	{
		$result = $this->db->query("insert into barang values ('null','$nama_barang','$foto_barang','$stok','$berat','$harga_beli','$harga_jual','$deskripsi','$id_jenis_barang')");
		return $result;
	}

	function ubah($id_barang,$nama_barang,$foto_barang,$stok,$berat,$harga_beli,$harga_jual,$deskripsi,$id_jenis_barang)
	{
		$result = $this->db->query("update barang set nama_barang='$nama_barang',stok='$stok',berat='$berat',harga_beli='$harga_beli',harga_jual='$harga_jual',deskripsi='$deskripsi',id_jenis_barang='$id_jenis_barang' where id_barang='$id_barang'");
		if($foto_barang){
			$result = $this->db->query("update barang set foto_barang='$foto_barang' where id_barang='$id_barang'");
		}
		return $result;
	}

	function hapus($id_barang)
	{
		$result = $this->db->query("delete from barang where id_barang='$id_barang'");
		return $result;
	}
}											
