<?php

class M_Pesanan_Pembeli extends CI_Model
{

    function tampil_pesanan($id_pembeli)
	{
		$result = $this->db->query("select *,pesanan.status status_pesanan from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			WHERE pembeli.id_pembeli='$id_pembeli'
			AND pesanan.status != 'Keranjang'");
		return $result;
	}

	function tampil_detail($id_pesanan)
	{
		$result = $this->db->query("select *,pesanan.status status_pesanan, detail_ongkir.status status_pengiriman, pembeli.nomor_telepon nomor_telepon_pembeli,
			alamat.nomor_telepon nomor_telepon_penerima from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			inner join pembayaran on pembayaran.id_pesanan=pesanan.id_pesanan
			inner join detail_ongkir on detail_ongkir.id_pesanan=pesanan.id_pesanan
			WHERE pesanan.id_pesanan='$id_pesanan'");
		return $result;
	}

	function tampil_detail_pesanan($id_pesanan)
	{
		$result = $this->db->query("select *,pesanan.status status_pesanan from pesanan
			inner join detail_pesanan on detail_pesanan.id_pesanan=pesanan.id_pesanan
			inner join barang on barang.id_barang=detail_pesanan.id_barang
			inner join jenis_barang on jenis_barang.id_jenis_barang=barang.id_jenis_barang
			WHERE pesanan.id_pesanan='$id_pesanan'");
		return $result;
	}

	function diterima($id_pesanan)
	{
		$result = $this->db->query("update pesanan set status='Selesai' where id_pesanan='$id_pesanan'");
		$result = $this->db->query("update detail_ongkir set status='Selesai' where id_pesanan='$id_pesanan'");
		return $result;
	}

}											
