<?php

class M_Alamat extends CI_Model
{
	
	function tampil($id_pembeli)
	{
		$result = $this->db->query("select * from alamat
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			where id_pembeli='$id_pembeli'");
		return $result;
    }
    
    function tampil_alamat($id_alamat)
	{
		$result = $this->db->query("select * from alamat
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			WHERE alamat.id_alamat='$id_alamat'
			LIMIT 1");
		return $result;
	}
    
    function alamat_pesanan($id_alamat)
	{
		$result = $this->db->query("select * from pesanan WHERE id_alamat='$id_alamat' LIMIT 1");
		return $result;
	}

	function simpan($nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan)
	{
		if($default=='1'){
			$result = $this->db->query("update alamat set default='0' where id_pembeli='$id_pembeli'");
		}
		$result = $this->db->query("insert into alamat values ('null','$nama_penerima','$nomor_telepon','$alamat','$default','$id_pembeli','$id_kecamatan')");
		return $result;
	}

	function ubah($id_alamat,$nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan)
	{
		if($default=='1'){
			$result = $this->db->query("update alamat set `default`='0' where id_pembeli='$id_pembeli'");
		}
		$result = $this->db->query("update alamat set nama_penerima='$nama_penerima',nomor_telepon='$nomor_telepon',alamat='$alamat',`default`='$default',id_kecamatan='$id_kecamatan' where id_alamat='$id_alamat'");
		return $result;
	}

	function hapus($id_alamat)
	{
		$result = $this->db->query("delete from alamat where id_alamat='$id_alamat'");
		return $result;
	}
}											
