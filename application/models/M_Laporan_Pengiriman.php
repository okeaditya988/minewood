<?php

class M_Laporan_Pengiriman extends CI_Model
{
	
	function transaksi($tanggal_mulai,$tanggal_selesai,$status)
	{
		$kondisi = '';
		if($status!='Semua'){
			$kondisi = "and detail_ongkir.status='$status'";
		}

		$result = $this->db->query("select *, detail_ongkir.status status_pengiriman from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			inner join detail_ongkir on detail_ongkir.id_pesanan=pesanan.id_pesanan
			where pesanan.tanggal_pesanan BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'
			and pesanan.status != 'Keranjang' and pesanan.status != 'Menunggu Pembayaran'
			$kondisi
			order by pesanan.tanggal_pesanan desc");
		return $result;
    }
    
}											
