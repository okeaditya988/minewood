<?php

class M_Keranjang extends CI_Model
{

	function tampil_alamat($id_pembeli)
	{
		$result = $this->db->query("select * from alamat
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			where id_pembeli='$id_pembeli'
			order by `default` desc limit 1");
		return $result;
	}

    function tampil($id_alamat)
	{
		$result = $this->db->query("select * from pesanan where pesanan.id_alamat='$id_alamat' and pesanan.status='Keranjang'");
		return $result;
	}

    function tampil_detail($id_alamat)
	{
		$result = $this->db->query("select * from detail_pesanan
			inner join pesanan on pesanan.id_pesanan=detail_pesanan.id_pesanan
			inner join barang on barang.id_barang=detail_pesanan.id_barang
			where pesanan.id_alamat='$id_alamat' and pesanan.status='Keranjang'");
		return $result;
	}

    function tampil_pesanan($id_pesanan)
	{
		$result = $this->db->query("select * from detail_ongkir where detail_ongkir.id_pesanan='$id_pesanan'");
		return $result;
	}

    function simpan($jumlah,$sub_total,$id_barang,$id_alamat)
	{
		$cek = $this->db->query("select * from pesanan where id_alamat='$id_alamat' and status='Keranjang' limit 1")->row();
		if(!$cek){
			$result = $this->db->query("insert into pesanan(id_pesanan,tanggal_pesanan,status,id_alamat) value('null',now(),'Keranjang','$id_alamat')");
			$id_pesanan = $this->db->insert_id();
		}else{
			$id_pesanan = $cek->id_pesanan;
		}

		$cek2 = $this->db->query("select * from detail_pesanan where id_pesanan='$id_pesanan' and id_barang='$id_barang' limit 1")->row();

		if($cek2){
			$result = $this->db->query("update detail_pesanan set jumlah=jumlah+$jumlah, sub_total=sub_total+$sub_total where id_pesanan='$id_pesanan' and id_barang='$id_barang'");
		}else{
			$result = $this->db->query("insert into detail_pesanan value('null','$jumlah','$sub_total','','$id_pesanan','$id_barang')");
		}

		$detail_pesanan = $this->db->query("select * from detail_pesanan where id_pesanan='$id_pesanan'")->result();
		$total = 0;
		$total_berat = 0;
		foreach($detail_pesanan as $key){
			$barang =  $this->db->query("select * from barang where id_barang='$key->id_barang'")->row();
			$total += $key->sub_total;
			$total_berat += ($jumlah*$barang->berat);
		}
		self::total($total, $total_berat, $id_alamat);

		return $result;
	}

    function perbarui($jumlah,$sub_total,$id_barang,$id_alamat)
	{
		$id_pesanan = $this->db->query("select * from pesanan where id_alamat='$id_alamat' and status='Keranjang' limit 1")->row()->id_pesanan;

		$result = $this->db->query("update detail_pesanan set jumlah=$jumlah, sub_total=$sub_total where id_pesanan='$id_pesanan' and id_barang='$id_barang'");

		return $result;
	}

	function total($total, $total_berat, $id_alamat)
	{
		$id_pesanan = $this->db->query("select * from pesanan where id_alamat='$id_alamat' and status='Keranjang' limit 1")->row()->id_pesanan;

		$result = $this->db->query("update pesanan set total_harga=$total, total_pesanan=$total, total_berat=$total_berat where id_pesanan='$id_pesanan'");
		return $result;
	}

	function update_kurir($id_alamat, $kode_kurir, $biaya_pengiriman)
	{
		$id_pesanan = $this->db->query("select * from pesanan where id_alamat='$id_alamat' and status='Keranjang' limit 1")->row()->id_pesanan;

		$result = $this->db->query("insert into detail_ongkir(id_detail_ongkir,kode_kurir,status,id_pesanan) value('null','$kode_kurir','Belum Dikirim','$id_pesanan')");

		$result = $this->db->query("update pesanan set total_ongkir='$biaya_pengiriman',total_pesanan=(total_pesanan+$biaya_pengiriman) where id_pesanan='$id_pesanan'");
		return $result;
	}

	function update_pesanan($id_pesanan,$order_id,$payment_type,$transaction_time,$pdf,$status)
	{
		$pesanan = $this->db->query("select * from pesanan where id_pesanan='$id_pesanan'")->row();

		$result = $this->db->query("insert into pembayaran value('null','$order_id','$payment_type',DATE_ADD(NOW(), INTERVAL 1 DAY),'$pesanan->total_pesanan','null','$pdf','$id_pesanan')");

		$result = $this->db->query("update pesanan set status='$status',tanggal_pesanan='$transaction_time' where id_pesanan='$id_pesanan'");
		return $result;
	}

    function hapus($id_barang,$id_alamat)
	{
		$id_pesanan = $this->db->query("select * from pesanan where id_alamat='$id_alamat' and status='Keranjang' limit 1")->row()->id_pesanan;
		if($id_barang){
			$result = $this->db->query("delete from detail_pesanan where id_barang not in ($id_barang) and id_pesanan='$id_pesanan'");
		}else{
			$result = $this->db->query("delete from detail_pesanan where id_pesanan='$id_pesanan'");
		}

		return $result;
	}

	function kurangi_stok($id_pesanan)
	{
		$detail_pesanan = $this->db->query("select * from detail_pesanan where id_pesanan='$id_pesanan'")->result();
		foreach($detail_pesanan as $key){
			$jumlah = $key->jumlah;
			$id_barang = $key->id_barang;
			$this->db->query("update barang set stok=stok-$jumlah where id_barang=$id_barang");
		}

		return $detail_pesanan;
	}

	function kurangi_stok2($order_id)
	{
		$pesanan = $this->db->query("select * from pembayaran where order_id_midtrans='$order_id'")->row()->id_pesanan;
		$detail_pesanan = $this->db->query("select * from detail_pesanan where id_pesanan='$pesanan'")->result();
		foreach($detail_pesanan as $key){
			$jumlah = $key->jumlah;
			$id_barang = $key->id_barang;
			$this->db->query("update barang set stok=stok-$jumlah where id_barang=$id_barang");
		}

		return $detail_pesanan;
	}

	function batal($id_pesanan)
	{
		$result = $this->db->query("update pesanan set total_pesanan=total_harga,total_ongkir=0,total_berat=0 where id_pesanan='$id_pesanan'");
		return $result;
	}

	function endpoint($order_id,$transaction_status){
		$pesanan = $this->db->query("select * from pembayaran where order_id_midtrans='$order_id'")->row()->id_pesanan;
		$result = $this->db->query("update pesanan set status='$transaction_status' where id_pesanan='$pesanan'");
		return $result;
	}

	function pembayaran($order_id,$transaction_time){
		$result = $this->db->query("update pembayaran set tanggal_pembayaran='$transaction_time' where order_id_midtrans='$order_id'");
		return $result;
	}

}											
