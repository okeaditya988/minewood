<?php

class M_Laporan_Pesanan extends CI_Model
{
	
	function transaksi($tanggal_mulai,$tanggal_selesai,$status)
	{
		$kondisi = '';
		if($status!='Semua'){
			$kondisi = "and pesanan.status='$status'";
		}

		$result = $this->db->query("select *, sum(jumlah*harga_jual)-sum(jumlah*harga_beli) as keuntungan, pesanan.status status_pesanan from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			inner join detail_pesanan on detail_pesanan.id_pesanan=pesanan.id_pesanan
			inner join barang on detail_pesanan.id_barang=detail_pesanan.id_barang
			where pesanan.tanggal_pesanan BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'
			$kondisi
			order by pesanan.tanggal_pesanan desc");
		return $result;
    }
    
}											
