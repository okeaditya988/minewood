<?php

class M_Laporan_Pembayaran extends CI_Model
{
	
	function transaksi($tanggal_mulai,$tanggal_selesai)
	{
		$result = $this->db->query("select *,pesanan.status status_pesanan from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			inner join pembayaran on pembayaran.id_pesanan=pesanan.id_pesanan
			where pesanan.status != 'Keranjang'
			and pesanan.tanggal_pesanan BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'
			order by pesanan.tanggal_pesanan desc");
		return $result;
    }
    
}											
