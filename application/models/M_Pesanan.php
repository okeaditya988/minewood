<?php

class M_Pesanan extends CI_Model
{
	
	function tampil()
	{
		$result = $this->db->query("select *,pesanan.status status_pesanan from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			where pesanan.status != 'Keranjang'
			order by pesanan.tanggal_pesanan desc");
		return $result;
    }
	
	function tampil_pesanan($id_pesanan)
	{
		$result = $this->db->query("select * from pesanan where id_pesanan='$id_pesanan'");
		return $result;
    }

	function ubah($id_pesanan,$no_resi)
	{
		$result = $this->db->query("update pesanan set no_resi='$no_resi', status_pesanan='Dalam Pengiriman' where id_pesanan='$id_pesanan'");
		return $result;
	}
    
	function batalkan($id_pesanan)
	{
		$result = $this->db->query("update pesanan set status='Batal' where id_pesanan='$id_pesanan'");
		$result = $this->db->query("update detail_ongkir set status='Batal' where id_pesanan='$id_pesanan'");
		return $result;
	}
}											
