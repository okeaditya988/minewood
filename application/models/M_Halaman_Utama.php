<?php

class M_Halaman_Utama extends CI_Model
{

    function barang_terbaru()
	{
		$result = $this->db->query("select * from barang LIMIT 12");
		return $result;
	}

    function barang_terlaris()
	{
		$result = $this->db->query("select *,SUM(jumlah) AS terlaris from detail_pesanan order by terlaris DESC LIMIT 10");
		return $result;
	}

	function barang_pilihan($id_jenis_barang)
	{
		$result = $this->db->query("select * from barang where id_jenis_barang='$id_jenis_barang' limit 10");
		return $result;
	}

}											
