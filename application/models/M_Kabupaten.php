<?php

class M_Kabupaten extends CI_Model
{
	
	function tampil()
	{
		$result = $this->db->query("select * from kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			order by provinsi.nama_provinsi asc,kabupaten.nama_kabupaten asc");
		return $result;
	}
    
    function cek_nama($nama,$id_provinsi)
	{
		$result = $this->db->query("select * from kabupaten WHERE nama_kabupaten='$nama' and id_provinsi='$id_provinsi' LIMIT 1");
		return $result;
	}
	
	function get_kabupaten($id_provinsi)
	{
		$result = $this->db->query("select * from kabupaten where id_provinsi='$id_provinsi'");
		return $result;
    }
    
    function tampil_kabupaten($id_kabupaten)
	{
		$result = $this->db->query("select * from kabupaten WHERE id_kabupaten='$id_kabupaten' LIMIT 1");
		return $result;
	}
    
    function kecamatan_kabupaten($id_kabupaten)
	{
		$result = $this->db->query("select * from kecamatan WHERE id_kabupaten='$id_kabupaten' LIMIT 1");
		return $result;
	}

	function simpan($nama_kabupaten,$id_provinsi)
	{
		$result = $this->db->query("insert into kabupaten values ('null','$nama_kabupaten','$id_provinsi')");
		return $result;
	}

	function ubah($id_kabupaten,$nama_kabupaten,$id_provinsi)
	{
		$result = $this->db->query("update kabupaten set nama_kabupaten='$nama_kabupaten',id_provinsi='$id_provinsi' where id_kabupaten='$id_kabupaten'");
		return $result;
	}

	function hapus($id_kabupaten)
	{
		$result = $this->db->query("delete from kabupaten where id_kabupaten='$id_kabupaten'");
		return $result;
	}
}											
