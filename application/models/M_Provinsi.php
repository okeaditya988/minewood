<?php

class M_Provinsi extends CI_Model
{
	
	function tampil()
	{
		$result = $this->db->query("select * from provinsi order by nama_provinsi asc");
		return $result;
    }
    
    function cek_nama($nama)
	{
		$result = $this->db->query("select * from provinsi WHERE nama_provinsi='$nama' LIMIT 1");
		return $result;
	}
    
    function tampil_provinsi($id_provinsi)
	{
		$result = $this->db->query("select * from provinsi WHERE id_provinsi='$id_provinsi' LIMIT 1");
		return $result;
	}
    
    function kabupaten_provinsi($id_provinsi)
	{
		$result = $this->db->query("select * from kabupaten WHERE id_provinsi='$id_provinsi' LIMIT 1");
		return $result;
	}

	function simpan($nama_provinsi)
	{
		$result = $this->db->query("insert into provinsi values ('null','$nama_provinsi')");
		return $result;
	}

	function ubah($id_provinsi,$nama_provinsi)
	{
		$result = $this->db->query("update provinsi set nama_provinsi='$nama_provinsi' where id_provinsi='$id_provinsi'");
		return $result;
	}

	function hapus($id_provinsi)
	{
		$result = $this->db->query("delete from provinsi where id_provinsi='$id_provinsi'");
		return $result;
	}
}											
