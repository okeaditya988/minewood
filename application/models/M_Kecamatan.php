<?php

class M_Kecamatan extends CI_Model
{
	
	function tampil()
	{
		$result = $this->db->query("select * from kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			order by provinsi.nama_provinsi asc,kabupaten.nama_kabupaten asc,kecamatan.nama_kecamatan asc");
		return $result;
	}
    
    function cek_nama($nama,$id_kabupaten)
	{
		$result = $this->db->query("select * from kecamatan WHERE nama_kecamatan='$nama' and id_kabupaten='$id_kabupaten' LIMIT 1");
		return $result;
	}
	
	function get_kecamatan($id_kabupaten)
	{
		$result = $this->db->query("select * from kecamatan where id_kabupaten='$id_kabupaten'");
		return $result;
    }
    
    function tampil_kecamatan($id_kecamatan)
	{
		$result = $this->db->query("select kecamatan.*,kabupaten.id_kabupaten,kabupaten.id_provinsi from kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			WHERE id_kecamatan='$id_kecamatan'
			LIMIT 1");
		return $result;
	}
    
    function alamat_kecamatan($id_kecamatan)
	{
		$result = $this->db->query("select * from alamat WHERE id_kecamatan='$id_kecamatan' LIMIT 1");
		return $result;
	}

	function simpan($nama_kecamatan,$id_kabupaten)
	{
		$result = $this->db->query("insert into kecamatan values ('null','$nama_kecamatan','$id_kabupaten')");
		return $result;
	}

	function ubah($id_kecamatan,$nama_kecamatan,$id_kabupaten)
	{
		$result = $this->db->query("update kecamatan set nama_kecamatan='$nama_kecamatan',id_kabupaten='$id_kabupaten' where id_kecamatan='$id_kecamatan'");
		return $result;
	}

	function hapus($id_kecamatan)
	{
		$result = $this->db->query("delete from kecamatan where id_kecamatan='$id_kecamatan'");
		return $result;
	}
}											
