<?php

class M_Dashboard extends CI_Model
{
	function tampil_stok_menipis()
	{
		$result = $this->db->query("select * from barang
			inner join jenis_barang on jenis_barang.id_jenis_barang=barang.id_jenis_barang
			where stok < 10 LIMIT 10");
		return $result;
	}

	function tampil_barang_terlaris()
	{
		$result = $this->db->query("select sum(jumlah) total,barang.nama_barang,jenis_barang.jenis_barang from detail_pesanan
			inner join pesanan on pesanan.id_pesanan=detail_pesanan.id_pesanan
			inner join barang on barang.id_barang=detail_pesanan.id_barang
			inner join jenis_barang on jenis_barang.id_jenis_barang=barang.id_jenis_barang
			where pesanan.status!='Batal' and pesanan.status!='Menunggu Pembayaran'
			group by barang.nama_barang,jenis_barang.jenis_barang
			order by total desc");
		return $result;
	}

	function produk_terjual()
	{
		$result = $this->db->query("select ifnull(sum(jumlah),0) total from detail_pesanan
			inner join pesanan on pesanan.id_pesanan=detail_pesanan.id_pesanan
			where pesanan.status!='Batal' and pesanan.status!='Menunggu Pembayaran'");
		return $result;
	}

	function total_pesanan()
	{
		$result = $this->db->query("select count(*) total from pesanan where pesanan.status!='Batal' and pesanan.status!='Menunggu Pembayaran'");
		return $result;
	}

	function total_keuntungan()
	{
		$result = $this->db->query("select sum(jumlah*(harga_jual-harga_beli)) total from detail_pesanan
			inner join pesanan on pesanan.id_pesanan=detail_pesanan.id_pesanan
			inner join barang on barang.id_barang=detail_pesanan.id_barang
			where pesanan.status!='Batal' and pesanan.status!='Menunggu Pembayaran'");
		return $result;
	}

	function total_pembeli()
	{
		$result = $this->db->query("select count(*) total from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			where pesanan.status!='Batal' and pesanan.status!='Menunggu Pembayaran'");
		return $result;
	}

	function tampil_pesanan()
	{
		$result = $this->db->query("select *,pesanan.status status_pesanan from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			where pesanan.status !='Batal' && pesanan.status != 'Keranjang'
			order by pesanan.id_pesanan desc
			limit 10");
		return $result;
	}
	
}											
