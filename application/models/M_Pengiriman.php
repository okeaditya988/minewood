<?php

class M_Pengiriman extends CI_Model
{
	
	function tampil()
	{
		$result = $this->db->query("select *,detail_ongkir.status status_pengiriman from pesanan
			inner join alamat on alamat.id_alamat=pesanan.id_alamat
			inner join pembeli on pembeli.id_pembeli=alamat.id_pembeli
			inner join kecamatan on kecamatan.id_kecamatan=alamat.id_kecamatan
			inner join kabupaten on kabupaten.id_kabupaten=kecamatan.id_kabupaten
			inner join provinsi on provinsi.id_provinsi=kabupaten.id_provinsi
			inner join detail_ongkir on detail_ongkir.id_pesanan=pesanan.id_pesanan
			where pesanan.status != 'Keranjang' and pesanan.status != 'Batal'
			order by pesanan.tanggal_pesanan desc");
		return $result;
    }
	
	function tampil_pesanan($id_pesanan)
	{
		$result = $this->db->query("select * from pesanan where id_pesanan='$id_pesanan'");
		return $result;
    }

	function ubah($id_pesanan,$no_resi)
	{
		$result = $this->db->query("update detail_ongkir set nomor_resi='$no_resi', status='Dalam Pengiriman' where id_pesanan='$id_pesanan'");
		$result = $this->db->query("update pesanan set status='Dalam Pengiriman' where id_pesanan='$id_pesanan'");
		return $result;
	}
    
}											
