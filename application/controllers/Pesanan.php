<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('level')!=TRUE || ($this->session->userdata('hak_akses')!='Administrator')){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('login');
        }
        $this->load->model('M_Pesanan');
        // $this->load->model('M_Profil');
        $this->load->helper('helper');
    }

    function index(){
		$data['tampil'] = $this->M_Pesanan->tampil()->result();
        $this->load->view('back/V_Pesanan',$data);
    }

    function tampil_pesanan(){
        $id_pesanan = $this->input->post('id_pesanan');
        $pesanan = $this->M_Pesanan->tampil_pesanan($id_pesanan)->row();
        echo json_encode($pesanan);
    }


	function batalkan()
	{
		$this->db->trans_start();
        $id_pesanan = $this->input->post('id');
        $return = $this->M_Pesanan->batalkan($id_pesanan);
		$this->db->trans_complete();
		if($return==0){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
	}

    // function ubah(){
	// 	$this->db->trans_start();
    //     $id_pesanan = $this->input->post('id_pesanan');
    //     $no_resi = $this->input->post('no_resi');
    //     $return = $this->M_Pesanan->ubah($id_pesanan,$no_resi);
	// 	$this->db->trans_complete();
	// 	if($return==0){
    //     	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
	// 	}else{
	// 		echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
	// 	}
    //     redirect('Pesanan');
    // }

	// function ubah_status(){
	// 	$this->db->trans_start();
    //     $id_pesanan = $this->input->post('id');
    //     $status = $this->input->post('status');
		
    //     $return = $this->M_Profil->ubah_status($id_pesanan,$status);

	// 	$this->db->trans_complete();
	// 	if($return==0){
    //     	echo json_encode("Sukses");
	// 	}else{
    //     	echo json_encode("Gagal",500);
	// 	}
    // }

}
