<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('level')!=TRUE){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('login');
        }
        $this->load->model('M_Dashboard');
        $this->load->helper('helper');
    }

    function index(){
		$data['tampil_barang_terlaris'] = $this->M_Dashboard->tampil_barang_terlaris()->result();
		$data['tampil_stok_menipis'] = $this->M_Dashboard->tampil_stok_menipis()->result();
		$data['tampil_pesanan'] = $this->M_Dashboard->tampil_pesanan()->result();
		$data['produk_terjual'] = $this->M_Dashboard->produk_terjual()->row();
		$data['total_pembeli'] = $this->M_Dashboard->total_pembeli()->row();
		$data['total_keuntungan'] = $this->M_Dashboard->total_keuntungan()->row();
		$data['total_pesanan'] = $this->M_Dashboard->total_pesanan()->row();
        $this->load->view('back/V_Dashboard',$data);
    }

}
