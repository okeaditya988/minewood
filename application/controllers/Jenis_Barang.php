<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_Barang extends CI_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
        $this->load->model('M_Jenis_Barang');
    }

    function index(){
        $jenis_barang = $this->M_Jenis_Barang->tampil()->result();
		
		foreach($jenis_barang as $key){
			$key->disabled = false;
			$barang_jenis_barang = $this->M_Jenis_Barang->barang_jenis_barang($key->id_jenis_barang)->row();
			if($barang_jenis_barang){
				$key->disabled = true;
			}
		}
		$data['tampil'] = $jenis_barang;
        $this->load->view('back/V_Jenis_Barang',$data);
    }

    function simpan(){
		$this->db->trans_start();
        $jenis_barang = $this->input->post('jenis_barang');
        $return = $this->M_Jenis_Barang->simpan($jenis_barang);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Jenis_Barang');
    }

    function tampil_jenis_barang(){
        $id_jenis_barang = $this->input->post('id_jenis_barang');
        $jenis_barang = $this->M_Jenis_Barang->tampil_jenis_barang($id_jenis_barang)->row();
        echo json_encode($jenis_barang);
    }

    function validasi(){
        $nama = $this->input->post('text');
		$hasil = $this->M_Jenis_Barang->cek_nama($nama)->row();
		if($hasil){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
    }

    function ubah(){
		$this->db->trans_start();
        $id_jenis_barang = $this->input->post('id_jenis_barang');
        $jenis_barang = $this->input->post('jenis_barang');
        $return = $this->M_Jenis_Barang->ubah($id_jenis_barang,$jenis_barang);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Jenis_Barang');
    }

    function hapus(){
		$this->db->trans_start();
        $id_jenis_barang = $this->input->post('id');
        $return = $this->M_Jenis_Barang->hapus($id_jenis_barang);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
        	echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Jenis_Barang');
    }
}
