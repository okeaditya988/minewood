<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengiriman extends CI_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('level')!=TRUE || ($this->session->userdata('hak_akses')!='Administrator')){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('login');
        }
        $this->load->model('M_Pengiriman');
        $this->load->helper('helper');
    }

    function index(){
		$data['tampil'] = $this->M_Pengiriman->tampil()->result();
        $this->load->view('back/V_Pengiriman',$data);
    }

    function ubah(){
		$this->db->trans_start();
        $id_pesanan = $this->input->post('id_pesanan');
        $no_resi = $this->input->post('no_resi');
        $return = $this->M_Pengiriman->ubah($id_pesanan,$no_resi);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Pengiriman');
    }

	// function ubah_status(){
	// 	$this->db->trans_start();
    //     $id_pesanan = $this->input->post('id');
    //     $status = $this->input->post('status');
		
    //     $return = $this->M_Profil->ubah_status($id_pesanan,$status);

	// 	$this->db->trans_complete();
	// 	if($return==0){
    //     	echo json_encode("Sukses");
	// 	}else{
    //     	echo json_encode("Gagal",500);
	// 	}
    // }

}
