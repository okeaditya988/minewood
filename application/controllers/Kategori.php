<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Kategori');
        $this->load->model('M_Jenis_Barang');
        $this->load->helper('helper');
    }

    function index(){
        $id_jenis_barang = $this->input->get('id');
        $nama_barang = $this->input->get('nama');
        if($id_jenis_barang){
            $data['tampil'] = $this->M_Kategori->tampil_barang_by_id_jenis_barang($id_jenis_barang)->result();
        }else{
            $nama_barang = strtolower($nama_barang);
            $data['tampil'] = $this->M_Kategori->tampil_barang_by_nama_barang($nama_barang)->result();
        }
        
        $data['tampil_kategori'] = $this->M_Jenis_Barang->tampil()->result();
        $data['jenis_barang'] = $this->M_Jenis_Barang->tampil_jenis_barang($id_jenis_barang)->row();
        $data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
        
        $this->load->view('front/V_Kategori',$data);
    }

}
