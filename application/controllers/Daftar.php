<?php
class Daftar extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('M_Daftar');
        $this->load->model('M_Jenis_Barang');
        $this->load->model('M_Provinsi');
        $this->load->model('M_Pembeli');
    }
 
    function index(){
        if(!empty($_SESSION['level_pembeli'])){
			$this->session->sess_destroy();
            redirect('Daftar');
        }else{
			$data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
			$data['tampil_provinsi'] = $this->M_Provinsi->tampil()->result();
			$this->load->view('front/V_Daftar',$data);
		}
    }

	function simpan(){
		$this->db->trans_start();
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $confirm = $this->input->post('confirm');
        $nama_penerima = $this->input->post('nama_penerima');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $nomor_telepon_penerima = $this->input->post('nomor_telepon_penerima');
        $alamat = $this->input->post('alamat');
        $id_kecamatan = $this->input->post('id_kecamatan');

		if($password != $confirm){
        	echo $this->session->set_flashdata('gagal','Password tidak sama!');
			redirect('Daftar');
		}

		$validasi = $this->M_Pembeli->validasi($email,'email')->row();
		if($validasi){
        	echo $this->session->set_flashdata('gagal','Email sudah digunakan!');
			redirect('Daftar');
		}

        $pembeli = $this->M_Daftar->simpan($nama,$email,$password,$nomor_telepon);

		$this->M_Daftar->simpan_alamat($nama_penerima,$nomor_telepon_penerima,$alamat,$pembeli,$id_kecamatan);

		$this->db->trans_complete();
		if($pembeli==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Login');
    }
 
}
