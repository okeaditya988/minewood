<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Provinsi');
    }

    function index(){
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
        $provinsi = $this->M_Provinsi->tampil()->result();
		
		foreach($provinsi as $key){
			$key->disabled = false;
			$kabupaten_provinsi = $this->M_Provinsi->kabupaten_provinsi($key->id_provinsi)->row();
			if($kabupaten_provinsi){
				$key->disabled = true;
			}
		}
		$data['tampil'] = $provinsi;
        $this->load->view('back/V_Provinsi',$data);
    }

    function simpan(){
		$this->db->trans_start();
        $nama_provinsi = $this->input->post('nama_provinsi');
        $return = $this->M_Provinsi->simpan($nama_provinsi);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Provinsi');
    }

    function tampil_provinsi(){
        $id_provinsi = $this->input->post('id_provinsi');
        $provinsi = $this->M_Provinsi->tampil_provinsi($id_provinsi)->row();
        echo json_encode($provinsi);
    }

    function validasi(){
        $nama = $this->input->post('text');
		$hasil = $this->M_Provinsi->cek_nama($nama)->row();
		if($hasil){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
    }

    function ubah(){
		$this->db->trans_start();
        $id_provinsi = $this->input->post('id_provinsi');
        $nama_provinsi = $this->input->post('nama_provinsi');
        $return = $this->M_Provinsi->ubah($id_provinsi,$nama_provinsi);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Provinsi');
    }

    function hapus(){
		$this->db->trans_start();
        $id_provinsi = $this->input->post('id');
        $return = $this->M_Provinsi->hapus($id_provinsi);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
        	echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Provinsi');
    }
}
