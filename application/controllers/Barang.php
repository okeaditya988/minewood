<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Barang');
        $this->load->model('M_Jenis_Barang');
        $this->load->helper('helper');
    }

    function index(){
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
        $barang = $this->M_Barang->tampil()->result();
		
		foreach($barang as $key){
			$key->disabled = false;
			$detail_pesanan_barang = $this->M_Barang->detail_pesanan_barang($key->id_barang)->row();
			if($detail_pesanan_barang){
				$key->disabled = true;
			}
		}
        $data['tampil'] = $barang;
        $data['tampil_jenis_barang'] = $this->M_Jenis_Barang->tampil()->result();
        $this->load->view('back/V_Barang',$data);
	}

    function simpan(){
		$this->db->trans_start();
        $nama_barang = $this->input->post('nama_barang');
        $stok = $this->input->post('stok');
        $berat = $this->input->post('berat');
        $harga_beli = $this->input->post('harga_beli');
        $harga_jual = $this->input->post('harga_jual');
        $deskripsi = $this->input->post('deskripsi');
        $id_jenis_barang = $this->input->post('id_jenis_barang');

		$foto_barang = upload('foto_barang','barang');

        $return = $this->M_Barang->simpan($nama_barang,$foto_barang,$stok,$berat,rupiahtoFloat($harga_beli),rupiahtoFloat($harga_jual),$deskripsi,$id_jenis_barang);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Barang');
    }

    function tampil_barang(){
        $id_barang = $this->input->post('id_barang');
        $barang = $this->M_Barang->tampil_barang($id_barang)->row();
        echo json_encode($barang);
    }

    function validasi(){
		$nama = $this->input->post('text');
		$id_jenis_barang = $this->input->post('select');
		$hasil = $this->M_Barang->cek_nama($nama,$id_jenis_barang)->row();
		if($hasil){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
    }

    function ubah(){
		$this->db->trans_start();
        $id_barang = $this->input->post('id_barang');
        $nama_barang = $this->input->post('nama_barang');
        $stok = $this->input->post('stok');
        $berat = $this->input->post('berat');
        $harga_beli = $this->input->post('harga_beli');
        $harga_jual = $this->input->post('harga_jual');
        $deskripsi = $this->input->post('deskripsi');
        $id_jenis_barang = $this->input->post('id_jenis_barang');

		$foto_barang = upload('foto_barang','barang');

        $return = $this->M_Barang->ubah($id_barang,$nama_barang,$foto_barang,$stok,$berat,rupiahtoFloat($harga_beli),rupiahtoFloat($harga_jual),$deskripsi,$id_jenis_barang);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Barang');
    }

    function hapus(){
		$this->db->trans_start();
        $id_barang = $this->input->post('id');
        $return = $this->M_Barang->hapus($id_barang);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
        	echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Barang');
    }
}
