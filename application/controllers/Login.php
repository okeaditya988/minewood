<?php
class Login extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('M_Login');
    }
 
    function index(){
        if(!empty($_SESSION['level_pembeli'])){
            redirect('Halaman_Utama');
        }else{
			$this->session->sess_destroy();
			$this->load->view('front/V_Login');
		}
    }
 
    function auth(){
        $email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
 
        $cek_user=$this->M_Login->auth_user($email,$password);
 
        if($cek_user->num_rows() > 0){
			$data=$cek_user->row_array();
		
            $this->session->set_userdata('level_pembeli',TRUE);
            $this->session->set_userdata('id_pembeli',$data['id_pembeli']);
            $this->session->set_userdata('nama_pembeli',$data['nama']);
            echo $this->session->set_flashdata('sukses','Selamat Datang '.$data['nama']);
            redirect('/');
        }else{
            $url=base_url().'index.php/Login';
            echo $this->session->set_flashdata('gagal','Username Atau Password Salah');
            redirect($url);
        }
 
    }
 
    function logout(){
        $this->session->sess_destroy();
        redirect('Halaman_Utama');
    }
 
}
