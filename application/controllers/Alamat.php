<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alamat extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Alamat');
        $this->load->model('M_Pembeli');
        $this->load->model('M_Provinsi');
    }

    function index(){
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
        $id_pembeli = $this->input->get('id');
		$pembeli = $this->M_Pembeli->tampil_pembeli($id_pembeli)->row();
		if(!$pembeli || !$id_pembeli){
			echo $this->session->set_flashdata('gagal','Data Pembeli tidak ditemukan!');
            redirect('Pembeli');
		}

		$alamat = $this->M_Alamat->tampil($id_pembeli)->result();

		foreach($alamat as $key){
			$key->disabled = false;
			$alamat_pesanan = $this->M_Alamat->alamat_pesanan($key->id_alamat)->row();
			if($alamat_pesanan){
				$key->disabled = true;
			}
		}

		$data['tampil'] = $alamat;
		$data['tampil_pembeli'] = $pembeli;
		$data['tampil_provinsi'] = $this->M_Provinsi->tampil()->result();
        $this->load->view('back/V_Alamat',$data);
    }

    function simpan(){
		$this->db->trans_start();
        $nama_penerima = $this->input->post('nama_penerima');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $alamat = $this->input->post('alamat');
        $default = $this->input->post('default');
        $id_pembeli = $this->input->post('id_pembeli');
        $id_kecamatan = $this->input->post('id_kecamatan');

        $return = $this->M_Alamat->simpan($nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Alamat?id='.$id_pembeli);
    }

    function tampil_alamat(){
        $id_alamat = $this->input->post('id_alamat');
        $alamat = $this->M_Alamat->tampil_alamat($id_alamat)->row();
        echo json_encode($alamat);
    }

    function ubah(){
		$this->db->trans_start();
        $id_alamat = $this->input->post('id_alamat');
        $nama_penerima = $this->input->post('nama_penerima');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $alamat = $this->input->post('alamat');
        $default = $this->input->post('default');
        $id_pembeli = $this->input->post('id_pembeli');
        $id_kecamatan = $this->input->post('id_kecamatan');

        $return = $this->M_Alamat->ubah($id_alamat,$nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Alamat?id='.$id_pembeli);
    }

    function hapus(){
		$this->db->trans_start();
        $id_alamat = $this->input->post('id');
        $alamat = $this->M_Alamat->tampil_alamat($id_alamat)->row();
        $return = $this->M_Alamat->hapus($id_alamat);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Alamat?id='.$alamat->id_pembeli);
    }
}
