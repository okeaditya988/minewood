<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('level_pembeli')!=TRUE){
            echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login');
        }
        $this->load->model('M_Profil');
        $this->load->model('M_Jenis_Barang');
        $this->load->model('M_Provinsi');
        $this->load->model('M_Alamat');
        $this->load->helper('helper');
    }

    function index(){
        $id_pembeli = $this->session->userdata('id_pembeli');
		$data['tampil'] = $this->M_Profil->tampil_profil($id_pembeli)->row();
		$data['tampil_alamat'] = $this->M_Profil->tampil_alamat($id_pembeli)->result();
		$data['tampil_provinsi'] = $this->M_Provinsi->tampil()->result();
        $data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
        $this->load->view('front/V_Profil',$data);
    }

    function tampil_alamat(){
        $id_alamat = $this->input->post('id_alamat');
        $alamat = $this->M_Alamat->tampil_alamat($id_alamat)->row();
        echo json_encode($alamat);
    }

    function ubah_profil(){
		$this->db->trans_start();
        $id_pembeli = $this->session->userdata('id_pembeli');
        $nama = $this->input->post('nama');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $email = $this->input->post('email');
        $email_lama = $this->input->post('email_lama');
		if($email_lama!=$email){
			$return = $this->M_Profil->cek_profil('email',$email)->row();
			if($return){
				echo $this->session->set_flashdata('gagal','Email Sudah Terdaftar!');
				redirect('Profil');
			}
		}

        $return = $this->M_Profil->ubah_profil($id_pembeli,$nama,$nomor_telepon,$email);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Profil');
    }

    function tambah_alamat(){
		$this->db->trans_start();
        $id_pembeli = $this->session->userdata('id_pembeli');
        $nama_penerima = $this->input->post('nama_penerima');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $alamat = $this->input->post('alamat');
        $default = $this->input->post('default');
        $id_kecamatan = $this->input->post('id_kecamatan');

        $return = $this->M_Profil->tambah_alamat($nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan);
		
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Profil');
    }

    function ubah_alamat(){
		$this->db->trans_start();
        $id_pembeli = $this->session->userdata('id_pembeli');
        $id_alamat = $this->input->post('id_alamat');
        $nama_penerima = $this->input->post('nama_penerima');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $alamat = $this->input->post('alamat');
        $default = $this->input->post('default');
        $id_kecamatan = $this->input->post('id_kecamatan');

        $return = $this->M_Profil->ubah_alamat($id_alamat,$nama_penerima,$nomor_telepon,$alamat,$default,$id_pembeli,$id_kecamatan);

		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Profil');
    }

    function ubah_password(){
		$this->db->trans_start();
        $id_pembeli = $this->session->userdata('id_pembeli');
        $new_password = $this->input->post('new_password');
        $confirm_password = $this->input->post('confirm_password');

		if($new_password!=$confirm_password){
        	echo $this->session->set_flashdata('gagal','Password Tidak Sama!');
			redirect('Profil');
		}

		$cek = $this->M_Profil->cek_password($id_pembeli,$new_password)->row();
		if(!$cek){
        	echo $this->session->set_flashdata('gagal','Password Lama Tidak Cocok!');
			redirect('Profil');
		}

        $return = $this->M_Profil->ubah_password($id_pembeli,$new_password);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Profil');
    }

	function ubah_status(){
		$this->db->trans_start();
        $id_pesanan = $this->input->post('id');

        $return = $this->M_Profil->ubah_status($id_pesanan,'Selesai');

		$this->db->trans_complete();
		if($return==0){
        	echo json_encode("Sukses");
		}else{
        	echo json_encode("Gagal",500);
		}
    }

    function hapus_alamat(){
		$this->db->trans_start();
        $id_alamat = $this->input->post('id');
        $alamat = $this->M_Alamat->tampil_alamat($id_alamat)->row();
        $return = $this->M_Alamat->hapus($id_alamat);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Profil');
    }

}
