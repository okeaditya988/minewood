<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_Pengiriman extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Laporan_Pengiriman');
        $this->load->helper('helper_helper');
        $this->load->library('Pdf');
    }

	public function index()
	{
        if($this->session->userdata('level')!=true){
            echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('login');
		}
		$this->load->view('back/V_Laporan_Pengiriman');
	}

	public function cetak(){
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_selesai = $this->input->post('tanggal_selesai');
		$status = $data['status'] = $this->input->post('status');

        $data['tanggal'] = longdate_indo($tanggal_mulai).' s.d. '.longdate_indo($tanggal_selesai);
		$data['tanggal_cetak'] = longdate_indo(date('Y-m-d'));
        
        $data['laporan'] = $laporan = $this->M_Laporan_Pengiriman->transaksi($tanggal_mulai,$tanggal_selesai,$status)->result();

		$html_content = $this->load->view('back/V_Cetak_Laporan_Pengiriman',$data,true);
			
        $this->pdf->set_paper('A4', 'potrait');
        $this->pdf->loadHtml($html_content);
        $this->pdf->render();
        $this->pdf->stream("Cetak_Pengiriman_".$data['tanggal'].".pdf", array("Attachment"=>0));
	}
}
