<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Kabupaten');
        $this->load->model('M_Provinsi');
    }

    function index(){
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
        $kabupaten = $this->M_Kabupaten->tampil()->result();
        $data['tampil_provinsi'] = $this->M_Provinsi->tampil()->result();
		
		foreach($kabupaten as $key){
			$key->disabled = false;
			$kecamatan_kabupaten = $this->M_Kabupaten->kecamatan_kabupaten($key->id_kabupaten)->row();
			if($kecamatan_kabupaten){
				$key->disabled = true;
			}
		}
        $data['tampil'] = $kabupaten;
        $this->load->view('back/V_Kabupaten',$data);
	}

    function simpan(){
		$this->db->trans_start();
        $nama_kabupaten = $this->input->post('nama_kabupaten');
        $id_provinsi = $this->input->post('id_provinsi');
        $return = $this->M_Kabupaten->simpan($nama_kabupaten,$id_provinsi);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Kabupaten');
    }
    
    function get_kabupaten(){
        $id_provinsi = $this->input->post('id_provinsi');
        $id_kabupaten = $this->input->post('id_kabupaten');
        $kabupaten = $this->M_Kabupaten->get_kabupaten($id_provinsi)->result();
        $data = '<option value="">-Pilih Kabupaten-</option>';
        
        foreach($kabupaten as $key){
            if($id_kabupaten==$key->id_kabupaten){
                $select = 'selected';
            }else{
                $select = '';
            }
            $data .= '<option value="'.$key->id_kabupaten.'" '.$select.'>'.$key->nama_kabupaten.'</option>';
        }
        echo json_encode($data);
    }

    function tampil_kabupaten(){
        $id_kabupaten = $this->input->post('id_kabupaten');
        $kabupaten = $this->M_Kabupaten->tampil_kabupaten($id_kabupaten)->row();
        echo json_encode($kabupaten);
    }

    function validasi(){
		$nama = $this->input->post('text');
		$id_provinsi = $this->input->post('select');
		$hasil = $this->M_Kabupaten->cek_nama($nama,$id_provinsi)->row();
		if($hasil){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
    }

    function ubah(){
		$this->db->trans_start();
        $id_kabupaten = $this->input->post('id_kabupaten');
        $nama_kabupaten = $this->input->post('nama_kabupaten');
        $id_provinsi = $this->input->post('id_provinsi');
        $return = $this->M_Kabupaten->ubah($id_kabupaten,$nama_kabupaten,$id_provinsi);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Kabupaten');
    }

    function hapus(){
		$this->db->trans_start();
        $id_kabupaten = $this->input->post('id');
        $return = $this->M_Kabupaten->hapus($id_kabupaten);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
        	echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Kabupaten');
    }
}
