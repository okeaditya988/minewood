<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keranjang extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Pembeli');
        $this->load->model('M_Barang');
        $this->load->model('M_Keranjang');
        $this->load->model('M_Jenis_Barang');
        $this->load->helper('helper');
		$params = array('server_key' => 'SB-Mid-server-Xh04YVxQoHLacyaf-bNvzows', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
    }

    function index(){
        if($this->session->userdata('level_pembeli')!=TRUE){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login');
        }
        $id_pembeli = $this->session->userdata('id_pembeli');
		$alamat = $this->M_Keranjang->tampil_alamat($id_pembeli)->row();

        $data['tampil_alamat'] = $alamat;
        $data['tampil'] = $this->M_Keranjang->tampil($alamat->id_alamat)->row();
        $data['tampil_detail'] = $this->M_Keranjang->tampil_detail($alamat->id_alamat)->result();
        $data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
        $data['ekspedisi'] = getOngkir($alamat->id_kecamatan);
        
        $this->load->view('front/V_Keranjang',$data);
    }

    function tambah(){
        if($this->session->userdata('level_pembeli')!=TRUE){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Daftar');
        }
		$this->db->trans_start();
        $id_pembeli = $this->session->userdata('id_pembeli');
		$alamat = $this->M_Keranjang->tampil_alamat($id_pembeli)->row();
        $id_barang = $this->input->get('id');
        $jumlah = $this->input->get('jumlah');

        $barang = $this->M_Barang->tampil_barang($id_barang)->row();
        if(!$barang){
        	echo $this->session->set_flashdata('gagal','Barang tidak ditemukan!');
            redirect('Keranjang');
        }
        
        $jumlah = $jumlah?$jumlah:1;
        $sub_total = $jumlah*$barang->harga_jual;

        $return = $this->M_Keranjang->simpan($jumlah,$sub_total,$id_barang,$alamat->id_alamat);

		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Keranjang');
    }

    function perbarui(){
        if($this->session->userdata('level_pembeli')!=TRUE){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Daftar');
        }
		$this->db->trans_start();
        $id_pembeli = $this->session->userdata('id_pembeli');
		$alamat = $this->M_Keranjang->tampil_alamat($id_pembeli)->row();
        $id_barang = $this->input->post('id_barang');
        $jumlah_barang = $this->input->post('jumlah_barang');

		$id_barangs = implode(",",$id_barang);
		$this->M_Keranjang->hapus($id_barangs,$alamat->id_alamat);

        $total = 0;
        $total_berat = 0;
        foreach($id_barang as $key => $value){
            $barang = $this->M_Barang->tampil_barang($id_barang[$key])->row();
            $sub_total = $jumlah_barang[$key]*$barang->harga_jual;
            $berat = $jumlah_barang[$key]*$barang->berat;
            $return = $this->M_Keranjang->perbarui($jumlah_barang[$key],$sub_total,$id_barang[$key],$alamat->id_alamat);
            $total += $sub_total;
            $total_berat += $berat;
        }

        $this->M_Keranjang->total($total, $total_berat, $alamat->id_alamat);

		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Keranjang Gagal Diperbarui!');
		}else{
			echo $this->session->set_flashdata('sukses','Keranjang Berhasil Diperbarui!');
		}
        redirect('Keranjang');
    }

    public function checkout(){
        if($this->session->userdata('level_pembeli')!=TRUE){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Daftar');
        }
        try{
			$this->db->trans_begin();

            $id_pembeli = $this->session->userdata('id_pembeli');
			$alamat = $this->M_Keranjang->tampil_alamat($id_pembeli)->row();
            $kode_kurir = $this->input->post('kode_kurir');
            $biaya_pengiriman = $this->input->post('biaya_pengiriman');
            $total_berat = $this->input->post('total_berat');

            $pesanan = $this->M_Keranjang->tampil($alamat->id_alamat)->row();
            $detail_keranjang = $this->M_Keranjang->tampil_detail($alamat->id_alamat)->result();

            $this->M_Keranjang->update_kurir($alamat->id_alamat,$kode_kurir,$biaya_pengiriman,$total_berat);

			$total = 0;
			foreach($detail_keranjang as $key){
                $total += $key->sub_total;
            }
			
			$total += $biaya_pengiriman;

			$transaction_details = array(
				'order_id' => time().'-'.rand().'-'.rand(),
				'gross_amount' => $total,
			);

			$transaction = array(
				'transaction_details' => $transaction_details
			);
            
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				echo json_encode(['status'=>'error','message'=>'Gagal checkout!']);
			}
			else
			{
				$this->db->trans_commit();
				$snapToken = $this->midtrans->getSnapToken($transaction);
				echo json_encode(['status'=>'success','token'=>$snapToken,'id_pesanan'=>$pesanan->id_pesanan]);
			}

			return true;
        }catch(Exception $e){
			$this->db->trans_rollback();
			echo json_encode(['status'=>'error','message'=>'Gagal checkout!']);
			return true;
        };
	}

    public function simpan_pembayaran(){
		$result_type = $this->input->post('result_type');
		$result_data = $this->input->post('result_data');
		$id_pesanan = $this->input->post('id_pesanan');
		$data = json_decode($result_data, true);

		if($result_type=='pending'){
			$status = 'Menunggu Pembayaran';
		}elseif($result_type='success'){
			$status = 'Menunggu Dikirim';
			$return = $this->M_Keranjang->kurangi_stok($id_pesanan);
		}else{
			$status = 'Batal';
		}

		if(isset($data['pdf_url'])){
			$pdf = $data['pdf_url'];
		}else{
			$pdf = '';
		}

		$return = $this->M_Keranjang->update_pesanan($id_pesanan,$data['order_id'],strtoupper($data['payment_type']),$data['transaction_time'],$pdf,$status);
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Transaksi Gagal!');
		}else{
			if($pdf){
				echo $this->session->set_flashdata('sukses','Transaksi Berhasil! Silahkan lakukan pembayaran!');
			}else{
				echo $this->session->set_flashdata('sukses','Transaksi Berhasil!');
			}
		}
		redirect('Keranjang');
	}

	public function batal(){
        if($this->session->userdata('level_pembeli')!=TRUE){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Daftar');
        }
        try{
			$this->db->trans_begin();

            $id_pembeli = $this->session->userdata('id_pembeli');
			$alamat = $this->M_Keranjang->tampil_alamat($id_pembeli)->row();
            $pesanan = $this->M_Keranjang->tampil($alamat->id_alamat)->row();

            $this->M_Keranjang->batal($pesanan->id_pesanan);

			$this->db->trans_commit();
			echo json_encode(['status'=>'success','message'=>'Pembayaran dibatalkan!']);

			return true;
        }catch(Exception $e){
			$this->db->trans_rollback();
			echo json_encode(['status'=>'error','message'=>'Gagal checkout!']);
			return true;
        };
	}

	public function endpoint(){
		$json_result = file_get_contents('php://input');
		$result = json_decode($json_result);

		$order_id = $result->order_id;
		$transaction_time = $result->transaction_time;
		$transaction_status = $result->transaction_status;
		if($transaction_status=='capture' || $transaction_status=='settlement'){
			$transaction_status = 'Menunggu Dikirim';
		}else{
			$transaction_status = 'Batal';
		}
		$return = $this->M_Keranjang->kurangi_stok2($order_id);
		$return = $this->M_Keranjang->endpoint($order_id,$transaction_status);
		$return = $this->M_Keranjang->pembayaran($order_id,$transaction_time);
		echo json_encode($return);
	}

	public function lacak(){
		$id_pesanan = $this->input->post('id_pesanan');
		$pesanan = $this->M_Keranjang->tampil_pesanan($id_pesanan)->row();
		$kode_kurir = explode(':',$pesanan->kode_kurir);
		$lacak = lacak($pesanan->nomor_resi,$kode_kurir[0]);
		echo json_encode($lacak);
	}

}
