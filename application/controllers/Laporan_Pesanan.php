<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_Pesanan extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Laporan_Pesanan');
        $this->load->helper('helper_helper');
        $this->load->library('Pdf');
    }

	public function index()
	{
        if($this->session->userdata('level')!=true){
            echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('login');
		}
		$this->load->view('back/V_Laporan_Pesanan');
	}

	public function cetak(){
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_selesai = $this->input->post('tanggal_selesai');
		$status = $data['status'] = $this->input->post('status');

        $data['tanggal'] = longdate_indo($tanggal_mulai).' s.d. '.longdate_indo($tanggal_selesai);
		$data['tanggal_cetak'] = longdate_indo(date('Y-m-d'));
        
        $data['laporan'] = $laporan = $this->M_Laporan_Pesanan->transaksi($tanggal_mulai,$tanggal_selesai,$status)->result();

		$keuntungan = 0;
		foreach($laporan as $key){
			$keuntungan += $key->keuntungan;
		}
		$data['keuntungan'] = $keuntungan;
		$html_content = $this->load->view('back/V_Cetak_Laporan_Pesanan',$data,true);
			
        $this->pdf->set_paper('A4', 'potrait');
        $this->pdf->loadHtml($html_content);
        $this->pdf->render();
        $this->pdf->stream("Cetak_Pesanan_".$data['tanggal'].".pdf", array("Attachment"=>0));
	}
}
