<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_Produk extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Barang');
        $this->load->model('M_Jenis_Barang');
        $this->load->helper('helper');
    }

    function index(){
        $id = $this->input->get('id');
        $data['produk'] = $this->M_Barang->tampil_barang($id)->row();
        $data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
        if(!$data['produk']){
            show_404();
        }
        $this->load->view('front/V_Detail_Produk',$data);
    }

}
