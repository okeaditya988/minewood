<?php
class Login_Admin extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('M_Login_Admin');
    }
 
    function index(){
        if(!empty($_SESSION['level'])){
            redirect('Dashboard');
        }else{
			$this->session->sess_destroy();
			$this->load->view('back/V_Login_Admin');
		}
    }
 
    function auth(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
 
        $cek_user=$this->M_Login_Admin->auth_user($username,$password);
 
        if($cek_user->num_rows() > 0){
			$data=$cek_user->row_array();
		
            $this->session->set_userdata('level',TRUE);
            $this->session->set_userdata('id_user',$data['id_user']);
            $this->session->set_userdata('nama_user',$data['nama_user']);
			$this->session->set_userdata('hak_akses',$data['hak_akses']);
            echo $this->session->set_flashdata('sukses','Selamat Datang '.$data['nama_user']);
			redirect('/Dashboard');
        }else{
            $url=base_url().'Login_Admin';
            echo $this->session->set_flashdata('gagal','Username Atau Password Salah');
            redirect($url);
        }
 
    }
 
    function logout(){
        $this->session->sess_destroy();
        redirect('Halaman_Utama');
    }
 
}
