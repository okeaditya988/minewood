<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_User');
    }

    function index(){
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
		$user = $this->M_User->tampil()->result();
		$data['tampil'] = $user;
        $this->load->view('back/V_User',$data);
    }

    function simpan(){
		$this->db->trans_start();
        $nama_user = $this->input->post('nama_user');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $hak_akses = $this->input->post('hak_akses');

        $return = $this->M_User->simpan($nama_user,$username,$password,$hak_akses);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('User');
    }

    function tampil_user(){
        $id_user = $this->input->post('id_user');
        $user = $this->M_User->tampil_user($id_user)->row();
        echo json_encode($user);
    }

    function validasi(){
        $text = $this->input->post('text');
        $type = $this->input->post('type');
		$user = $this->M_User->validasi($text,$type)->row();
		if($user){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
    }

    function ubah(){
		$this->db->trans_start();
        $id_user = $this->input->post('id_user');
        $nama_user = $this->input->post('nama_user');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
		$hak_akses = $this->input->post('hak_akses');

        $return = $this->M_User->ubah($id_user,$nama_user,$username,$password,$hak_akses);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
			if($id_user==$this->session->userdata('id_user')){
				redirect('Login');
			}
		}
        redirect('User');
    }

    function hapus(){
		$this->db->trans_start();
        $id_user = $this->input->post('id');
        $return = $this->M_User->hapus($id_user);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
			if($id_user==$this->session->userdata('id_user')){
				echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
				redirect('Login');
			}
		}
        redirect('User');
    }
}
