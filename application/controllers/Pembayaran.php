<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('level')!=TRUE || ($this->session->userdata('hak_akses')!='Administrator')){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('login');
        }
        $this->load->model('M_Pembayaran');
        $this->load->helper('helper');
    }

    function index(){
		$data['tampil'] = $this->M_Pembayaran->tampil()->result();
        $this->load->view('back/V_Pembayaran',$data);
    }
}
