<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Halaman_Utama extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Halaman_Utama');
        $this->load->model('M_Jenis_Barang');
        $this->load->model('M_Barang');
        $this->load->helper('helper');
    }

    function index(){
        $terlaris = $this->M_Halaman_Utama->barang_terlaris()->result();
        $barang_terlaris = array();
        foreach($terlaris as $key){
            $barang_terlaris[] = $this->M_Barang->tampil_barang($key->id_barang)->row();
        }
        $data['barang_terlaris'] = $barang_terlaris;
        $data['barang_terbaru'] = $this->M_Halaman_Utama->barang_terbaru()->result();
        $data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
        $this->load->view('front/V_Halaman_Utama',$data);
        // $this->load->view('front/V_Halaman_Utama');
    }

}
