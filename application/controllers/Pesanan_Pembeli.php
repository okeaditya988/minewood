<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan_Pembeli extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Profil');
        $this->load->model('M_Pesanan_Pembeli');
        $this->load->model('M_Jenis_Barang');
        $this->load->helper('helper');
    }

    function index(){
        if($this->session->userdata('level_pembeli')!=TRUE){
            echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login');
        }
        $id_pembeli = $this->session->userdata('id_pembeli');
		$data['tampil'] = $this->M_Profil->tampil_profil($id_pembeli)->row();
		$data['tampil_pesanan'] = $this->M_Pesanan_Pembeli->tampil_pesanan($id_pembeli)->result();
        $data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
        $this->load->view('front/V_Pesanan_Pembeli',$data);
    }

	function detail(){
        $id_pesanan = $this->input->get('id');

		$data['tampil'] = $pesanan = $this->M_Pesanan_Pembeli->tampil_detail($id_pesanan)->row();

		if(!$pesanan){
			show_404();
		}

		$data['tampil_detail'] = $this->M_Pesanan_Pembeli->tampil_detail_pesanan($id_pesanan)->result();

        $data['kategori'] = $this->M_Jenis_Barang->tampil()->result();
        $this->load->view('front/V_Detail_Pesanan',$data);
	}

	function diterima()
	{
		$this->db->trans_start();
        $id_pesanan = $this->input->post('id');
        $return = $this->M_Pesanan_Pembeli->diterima($id_pesanan);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diterima!');
		}else{
        	echo $this->session->set_flashdata('sukses','Data Berhasil Diterima!');
		}
        redirect('Pesanan_Pembeli');
	}

}
