<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembeli extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Pembeli');
    }

    function index(){
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
		$pembeli = $this->M_Pembeli->tampil()->result();

		foreach($pembeli as $key){
			$key->disabled = false;
			$alamat_pembeli = $this->M_Pembeli->alamat_pembeli($key->id_pembeli)->row();
			if($alamat_pembeli){
				$key->disabled = true;
			}
		}

		$data['tampil'] = $pembeli;
        $this->load->view('back/V_Pembeli',$data);
    }

    function simpan(){
		$this->db->trans_start();
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $status = $this->input->post('status');

        $return = $this->M_Pembeli->simpan($nama,$email,$password,$nomor_telepon,$status);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Pembeli');
    }

    function tampil_pembeli(){
        $id_pembeli = $this->input->post('id_pembeli');
        $pembeli = $this->M_Pembeli->tampil_pembeli($id_pembeli)->row();
        echo json_encode($pembeli);
    }

    function validasi(){
        $text = $this->input->post('text');
        $type = $this->input->post('type');
		$pembeli = $this->M_Pembeli->validasi($text,$type)->row();
		if($pembeli){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
    }

    function ubah(){
		$this->db->trans_start();
        $id_pembeli = $this->input->post('id_pembeli');
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $status = $this->input->post('status');

        $return = $this->M_Pembeli->ubah($id_pembeli,$nama,$email,$password,$nomor_telepon,$status);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Pembeli');
    }

    function hapus(){
		$this->db->trans_start();
        $id_pembeli = $this->input->post('id');
        $return = $this->M_Pembeli->hapus($id_pembeli);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Pembeli');
    }
}
