<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_Kecamatan');
        $this->load->model('M_Kabupaten');
        $this->load->model('M_Provinsi');
    }

    function index(){
        if($this->session->userdata('level')!=TRUE || $this->session->userdata('hak_akses')!='Administrator'){
            	echo $this->session->set_flashdata('gagal','Anda Belum Login/Tidak Memiliki Hak Akses!');
            redirect('Login_Admin');
        }
        $kecamatan = $this->M_Kecamatan->tampil()->result();
        $data['tampil_provinsi'] = $this->M_Provinsi->tampil()->result();
		
		foreach($kecamatan as $key){
			$key->disabled = false;
			$alamat_kecamatan = $this->M_Kecamatan->alamat_kecamatan($key->id_kecamatan)->row();
			if($alamat_kecamatan){
				$key->disabled = true;
			}
		}
        $data['tampil'] = $kecamatan;
        $this->load->view('back/V_Kecamatan',$data);
	}

    function simpan(){
		$this->db->trans_start();
        $nama_kecamatan = $this->input->post('nama_kecamatan');
        $id_kabupaten = $this->input->post('id_kabupaten');
        $return = $this->M_Kecamatan->simpan($nama_kecamatan,$id_kabupaten);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Disimpan!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Disimpan!');
		}
        redirect('Kecamatan');
    }
    
    function get_kecamatan(){
        $id_kabupaten = $this->input->post('id_kabupaten');
        $id_kecamatan = $this->input->post('id_kecamatan');
        $kabupaten = $this->M_Kecamatan->get_kecamatan($id_kabupaten)->result();
        $data = '<option value="">-Pilih Kecamatan-</option>';
        
        foreach($kabupaten as $key){
            if($id_kecamatan==$key->id_kecamatan){
                $select = 'selected';
            }else{
                $select = '';
            }
            $data .= '<option value="'.$key->id_kecamatan.'" '.$select.'>'.$key->nama_kecamatan.'</option>';
        }
        echo json_encode($data);
    }

    function tampil_kecamatan(){
        $id_kecamatan = $this->input->post('id_kecamatan');
        $kecamatan = $this->M_Kecamatan->tampil_kecamatan($id_kecamatan)->row();
        echo json_encode($kecamatan);
    }

    function validasi(){
		$nama = $this->input->post('text');
		$id_kabupaten = $this->input->post('select');
		$hasil = $this->M_Kecamatan->cek_nama($nama,$id_kabupaten)->row();
		if($hasil){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
    }

    function ubah(){
		$this->db->trans_start();
        $id_kecamatan = $this->input->post('id_kecamatan');
        $nama_kecamatan = $this->input->post('nama_kecamatan');
        $id_kabupaten = $this->input->post('id_kabupaten');
        $return = $this->M_Kecamatan->ubah($id_kecamatan,$nama_kecamatan,$id_kabupaten);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Diubah!');
		}else{
			echo $this->session->set_flashdata('sukses','Data Berhasil Diubah!');
		}
        redirect('Kecamatan');
    }

    function hapus(){
		$this->db->trans_start();
        $id_kecamatan = $this->input->post('id');
        $return = $this->M_Kecamatan->hapus($id_kecamatan);
		$this->db->trans_complete();
		if($return==0){
        	echo $this->session->set_flashdata('gagal','Data Gagal Dihapus!');
		}else{
        	echo $this->session->set_flashdata('sukses','Data Berhasil Dihapus!');
		}
        redirect('Kecamatan');
    }
}
