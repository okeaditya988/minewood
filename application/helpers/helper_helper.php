<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
      
    if ( ! function_exists('date_indo'))
    {
        function date_indo($tgl)
        {
            $tgl = date("Y-m-d", strtotime($tgl));
            $ubah = gmdate($tgl, time()+60*60*8);
            $pecah = explode("-",$ubah);
            $tanggal = $pecah[2];
            $bulan = bulan($pecah[1]);
            $tahun = $pecah[0];
            return $bulan.' '.$tahun;
        }
    }
      
    if ( ! function_exists('date_indo_reverse'))
    {
        function date_indo_reverse($tgl)
        {
            $pecah = explode(" ",$tgl);
            $bulan = bulan_reverse($pecah[0]);
            $tahun = $pecah[1];
            return $tahun.'-'.$bulan.'-01';
        }
    }
      
    if ( ! function_exists('tahun_indo'))
    {
        function tahun_indo($tgl)
        {
			if($tgl!='0000-00-00'){
				$tgl = date("Y-m-d", strtotime($tgl));
				$ubah = gmdate($tgl, time()+60*60*8);
				$pecah = explode("-",$ubah);
				$tahun = $pecah[0];
				return $tahun;
			}
			return '';
        }
    }
      
    if ( ! function_exists('bulan'))
    {
        function bulan($bln)
        {
            switch ($bln)
            {
                case 1:
                    return "Januari";
                    break;
                case 2:
                    return "Februari";
                    break;
                case 3:
                    return "Maret";
                    break;
                case 4:
                    return "April";
                    break;
                case 5:
                    return "Mei";
                    break;
                case 6:
                    return "Juni";
                    break;
                case 7:
                    return "Juli";
                    break;
                case 8:
                    return "Agustus";
                    break;
                case 9:
                    return "September";
                    break;
                case 10:
                    return "Oktober";
                    break;
                case 11:
                    return "November";
                    break;
                case 12:
                    return "Desember";
                    break;
            }
        }
	}
	

      
    if ( ! function_exists('bulan_reverse'))
    {
        function bulan_reverse($bln)
        {
            switch ($bln)
            {
                case "Januari":
                    return "01";
                    break;
                case "Februari":
                    return "02";
                    break;
                case "Maret":
                    return "03";
                    break;
                case "April":
                    return "04";
                    break;
                case "Mei":
                    return "05";
                    break;
                case "Juni":
                    return "06";
                    break;
                case "Juli":
                    return "07";
                    break;
                case "Agustus":
                    return "08";
                    break;
                case "September":
                    return "09";
                    break;
                case "Oktober":
                    return "10";
                    break;
                case "November":
                    return "11";
                    break;
                case "Desember":
                    return "12";
                    break;
            }
        }
    }
 
    //Format Shortdate
    if ( ! function_exists('shortdate_indo'))
    {
        function shortdate_indo($tgl)
        {
            $tgl = date("Y-m-d", strtotime($tgl));
            $ubah = gmdate($tgl, time()+60*60*8);
            $pecah = explode("-",$ubah);
            $tanggal = $pecah[2];
            $bulan = short_bulan($pecah[1]);
            $tahun = $pecah[0];
            return $tanggal.'/'.$bulan.'/'.$tahun;
        }
    }
      
    if ( ! function_exists('short_bulan'))
    {
        function short_bulan($bln)
        {
            switch ($bln)
            {
                case 1:
                    return "01";
                    break;
                case 2:
                    return "02";
                    break;
                case 3:
                    return "03";
                    break;
                case 4:
                    return "04";
                    break;
                case 5:
                    return "05";
                    break;
                case 6:
                    return "06";
                    break;
                case 7:
                    return "07";
                    break;
                case 8:
                    return "08";
                    break;
                case 9:
                    return "09";
                    break;
                case 10:
                    return "10";
                    break;
                case 11:
                    return "11";
                    break;
                case 12:
                    return "12";
                    break;
            }
        }
    }
 
    //Format Medium date
    if ( ! function_exists('mediumdate_indo'))
    {
        function mediumdate_indo($tgl)
        {
            $tgl = date("Y-m-d", strtotime($tgl));
            $ubah = gmdate($tgl, time()+60*60*8);
            $pecah = explode("-",$ubah);
            $tanggal = $pecah[2];
            $bulan = medium_bulan($pecah[1]);
            $tahun = $pecah[0];
            return $tanggal.'-'.$bulan.'-'.$tahun;
        }
    }
      
    if ( ! function_exists('medium_bulan'))
    {
        function medium_bulan($bln)
        {
            switch ($bln)
            {
                case 1:
                    return "Jan";
                    break;
                case 2:
                    return "Feb";
                    break;
                case 3:
                    return "Mar";
                    break;
                case 4:
                    return "Apr";
                    break;
                case 5:
                    return "Mei";
                    break;
                case 6:
                    return "Jun";
                    break;
                case 7:
                    return "Jul";
                    break;
                case 8:
                    return "Ags";
                    break;
                case 9:
                    return "Sep";
                    break;
                case 10:
                    return "Okt";
                    break;
                case 11:
                    return "Nov";
                    break;
                case 12:
                    return "Des";
                    break;
            }
        }
    }
     
    //Long date indo Format
    if ( ! function_exists('longdate_indo'))
    {
        function longdate_indo($tanggal)
        {
            $tanggal = date("Y-m-d", strtotime($tanggal)); 
            $ubah = gmdate($tanggal, time()+60*60*8);
            $pecah = explode("-",$ubah);
            $tgl = $pecah[2];
            $bln = $pecah[1];
            $thn = $pecah[0];
            $bulan = bulan($pecah[1]);
      
            $nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
            $nama_hari = "";
            if($nama=="Sunday") {$nama_hari="Minggu";}
            else if($nama=="Monday") {$nama_hari="Senin";}
            else if($nama=="Tuesday") {$nama_hari="Selasa";}
            else if($nama=="Wednesday") {$nama_hari="Rabu";}
            else if($nama=="Thursday") {$nama_hari="Kamis";}
            else if($nama=="Friday") {$nama_hari="Jumat";}
            else if($nama=="Saturday") {$nama_hari="Sabtu";}
            return $nama_hari.', '.$tgl.' '.$bulan.' '.$thn;
        }
    }
     
    //TIme indo Format
    if ( ! function_exists('time_indo'))
    {
        function time_indo($tanggal)
        {
            $waktu = date("H:i", strtotime($tanggal)); 
            return $waktu;
        }
    }
	
	function formatRupiah($angka)
	{
        $hasil_rupiah = number_format($angka,0,',','.');
        return $hasil_rupiah;
	}

	function rupiahtoFloat($angka)
	{
		return str_replace('.', '', $angka);
	}

    function upload($file,$jenis){
        if(isset($_FILES[$file])){
            if($_FILES[$file]['size'] != 0){
                $CI =& get_instance();
                $CI->load->library('upload');
                $config['upload_path'] = './uploads/'.$jenis;
                $config['allowed_types'] = 'jpg|png|jpeg'; 
                $config['max_size']  = '2048';
                $config['overwrite']  = true;
                $config['file_name']  = time();     
                $CI->upload->initialize($config);
                if($CI->upload->do_upload($file)){
                    return $CI->upload->file_name;
                }
            }
        }
		return null;
    }

    function getOngkir($id_kabupaten_pelanggan)
    {
        $id_kabupaten_toko = '344';
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=".$id_kabupaten_toko."&originType=city&destination=".$id_kabupaten_pelanggan."&destinationType=subdistrict&weight=1000&courier=jne:pos:tiki:sicepat:jnt:ninja:anteraja",
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 2bb29125e95be025d94ea7ea03726423"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $return = json_decode($response, true);
        $data = array();
        $i = 0;

        foreach($return['rajaongkir']['results'] as $key2){
            foreach($key2['costs'] as $key3){
                $data[$i]['kode_layanan_ekspedisi'] = $key2['code'];
                $data[$i]['layanan_ekspedisi'] = $key2['name'];
                $data[$i]['kode_jenis_layanan'] = $key3['service'];
                $data[$i]['jenis_layanan'] = $key3['description'];
                $data[$i]['harga'] = $key3['cost'][0]['value'];
                $data[$i]['estimasi'] = $key3['cost'][0]['etd'];
                $i++;
            }
        }

        if ($err) {
            return 'error';
        } else {
            return $data;
        }
    }

	function lacak($nomor_resi,$jasa_ekspedisi)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "waybill=".$nomor_resi."&courier=".$jasa_ekspedisi,
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 2bb29125e95be025d94ea7ea03726423"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $return = json_decode($response, true);

        if ($err) {
            return 'error';
        } else {
            return $return['rajaongkir'];
        }
    }
