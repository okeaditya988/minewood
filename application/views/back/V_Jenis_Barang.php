<!DOCTYPE html>
<html lang="en">

<head>

	<title>Jenis Barang - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Jenis Barang</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Jenis Barang</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Jenis Barang</div>
								<button class="btn btn-lg btn-primary p-2 btn-tambah" type="button"><i class="fa fa-plus"></i> Tambah</button>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Jenis Barang</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>	
													<td><?php echo $key->jenis_barang;?></td>
													<td style="width: 10%;">
														<div class="d-flex">
															<button class="btn btn-warning btn-ubah p-2 mr-2" value="<?php echo $key->id_jenis_barang;?>">Ubah</button>
															<?php if($key->disabled==false){?>
																<button class="btn btn-danger btn-hapus p-2" value="<?php echo $key->id_jenis_barang;?>">Hapus</button>
															<?php }?>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>
	
    <div class="modal fade modal-tampil" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"></h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<form class="form-horizontal form-label-left" method="POST" autocomplete="off">
					<div class="modal-body">
						<input name="id_jenis_barang" type="hidden" class="id_jenis_barang">
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Jenis Barang <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="jenis_barang" type="text" class="form-control jenis_barang" maxlength="50" placeholder="Masukkan Jenis Barang" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-danger btn-simpan">Simpan</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<?php $this->load->view('back/partials/script');?>

    <script>
		$(".btn-tambah").on("click", function(){
			$("#myModalLabel").html('Tambah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Jenis_Barang/simpan');
			clear();
			$(".jenis_barang").attr("old","");
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-ubah", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Ubah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Jenis_Barang/ubah');
			clear();
			get_data(id);
			$(".modal-tampil").modal("show");
		});

		$(document).on("click",".btn-hapus", function(){
			var id = $(this).attr("value");
			hapus(id,'Jenis_Barang');
		});

		$(".jenis_barang").on("keyup", function(){
			var text = $(this).val();
			var jenis_barang_old = $(this).attr("old");
			$.ajax({
				url : "<?php echo base_url();?>"+'Jenis_Barang/validasi',
				type: "POST",
				data: {
					text:text
				},
				dataType: 'json',
				success: function(respond){
					if(respond==true && jenis_barang_old!=text){
						notify('fa fa-times', 'danger', 'Peringatan', 'Jenis Barang sudah digunakan');
						$(".btn-simpan").prop("disabled",true);
					}else{
						$(".btn-simpan").prop("disabled",false);
					}
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		function get_data(id_jenis_barang){
			$.ajax({
				url : "<?php echo base_url();?>"+'Jenis_Barang/tampil_jenis_barang',
				type: "POST",
				data: {
					id_jenis_barang:id_jenis_barang
				},
				dataType: 'json',
				success: function(respond){
					$(".id_jenis_barang").val(respond.id_jenis_barang);
					$(".jenis_barang").val(respond.jenis_barang);
					$(".jenis_barang").attr("old",respond.jenis_barang);
				},
				error: function() {
				console.log('Error');
				}
			});
		};
	</script>
</body>
</html>
