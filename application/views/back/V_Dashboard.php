<!DOCTYPE html>
<html lang="en">

<head>

	<title>User - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Dashboard</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Dashboard</a></li>
					</ol>
                </div>
				
                <div class="row">
					<div class="col-xl-3 col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body mr-3">
										<h2 class="fs-34 text-black font-w600"><?php echo $total_pembeli->total?></h2>
										<span>Total Pembeli</span>
									</div>
									<i class="fa fa-user fa-5x text-primary"></i>
								</div>
							</div>
							<div class="progress  rounded-0" style="height:4px;">
								<div class="progress-bar rounded-0 bg-secondary progress-animated" style="width: 100%; height:4px;" role="progressbar"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body mr-3">
										<h2 class="fs-34 text-black font-w600"><?php echo $total_pesanan->total?></h2>
										<span>Total Penjualan</span>
									</div>
									<i class="fa fa-shopping-cart fa-5x text-primary"></i>
								</div>
							</div>
							<div class="progress  rounded-0" style="height:4px;">
								<div class="progress-bar rounded-0 bg-secondary progress-animated" style="width: 100%; height:4px;" role="progressbar"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body mr-3">
										<h2 class="fs-34 text-black font-w600"><?php echo $produk_terjual->total?></h2>
										<span>Produk Terjual</span>
									</div>
									<i class="fas fa-guitar fa-5x text-primary"></i>
								</div>
							</div>
							<div class="progress  rounded-0" style="height:4px;">
								<div class="progress-bar rounded-0 bg-secondary progress-animated" style="width: 100%; height:4px;" role="progressbar"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body mr-3">
										<h4 class="fs-24 text-black font-w600">Rp. <?php echo formatRupiah($total_keuntungan->total)?></h4>
										<span>Total Keuntungan</span>
									</div>
									<i class="fa fa-money fa-3x text-primary"></i>
								</div>
							</div>
							<div class="progress rounded-0" style="height:4px;">
								<div class="progress-bar rounded-0 bg-secondary progress-animated" style="width: 100%; height:4px;" role="progressbar"></div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="card">
							<div class="card-header d-flex justify-content-between align-items-center">
								<h4 class="card-title">10 Barang Terlaris</h4>
							</div>
							<div class="card-body px-0 pb-0">
								<div class="table-responsive">
									<table class='table mb-0'>
										<thead class="table-primary">
											<tr>
												<th>No</th>
												<th>Nama Barang</th>
												<th>Kategori Barang</th>
												<th>Banyak Terjual</th>
											</tr>
										</thead>
										<tbody>
											<?php if(count($tampil_barang_terlaris)>0){ $no=1; foreach($tampil_barang_terlaris as $key){?>
												<tr>
													<td><?php echo $no;?></td>	
													<td><?php echo $key->nama_barang;?></td>
													<td><?php echo $key->jenis_barang;?></td>
													<td><?php echo $key->total;?></td>
												</tr>
											<?php $no++; }}else{?>
												<tr>
													<td colspan="4" class="text-center">Tidak Ada Stok Barang Menipis</td>	
												</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="card">
							<div class="card-header d-flex justify-content-between align-items-center">
								<h4 class="card-title">10 Stok Barang Menipis</h4>
							</div>
							<div class="card-body px-0 pb-0">
								<div class="table-responsive">
									<table class='table mb-0'>
										<thead class="table-primary">
											<tr>
												<th>No</th>
												<th>Nama Barang</th>
												<th>Kategori Barang</th>
												<th>Stok</th>
											</tr>
										</thead>
										<tbody>
											<?php if(count($tampil_stok_menipis)>0){ $no=1; foreach($tampil_stok_menipis as $key){?>
												<tr>
													<td><?php echo $no;?></td>	
													<td><?php echo $key->nama_barang;?></td>
													<td><?php echo $key->jenis_barang;?></td>
													<td><?php echo $key->stok;?></td>
												</tr>
											<?php $no++; }}else{?>
												<tr>
													<td colspan="4" class="text-center">Tidak Ada Stok Barang Menipis</td>	
												</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-12">
						<div class="card">
							<div class="card-header d-flex justify-content-between align-items-center">
								<h4 class="card-title">10 Penjualan Terakhir</h4>
							</div>
							<div class="card-body px-0 pb-0">
								<div class="table-responsive">
									<table class='table mb-0'>
										<thead class="table-primary">
											<tr>
												<th>Tanggal Transaksi</th>
												<th>Nama Pembeli</th>
												<th>Total Pembelian</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php if(count($tampil_pesanan)>0){ foreach($tampil_pesanan as $key){?>
												<tr>
													<th><?php echo longdate_indo($key->tanggal_pesanan)?></th>
													<th><?php echo $key->nama?></th>
													<th class="text-right">Rp. <?php echo formatRupiah($key->total_pesanan)?></th>
													<th><?php echo $key->status_pesanan?></th>
												</tr>
											<?php }}else{?>
												<tr>
													<td colspan="4" class="text-center">Belum Ada Transaksi</td>	
												</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>
	
	<?php $this->load->view('back/partials/script');?>

</body>
</html>
