<!DOCTYPE html>
<html lang="en">

<head>

	<title>User - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>User</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">User</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data User</div>
								<button class="btn btn-lg btn-primary p-2 btn-tambah" type="button"><i class="fa fa-plus"></i> Tambah</button>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama User</th>
												<th>Username</th>
												<th>Hak Akses</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>	
													<td><?php echo $key->nama_user;?></td>
													<td><?php echo $key->username;?></td>
													<td><?php echo $key->hak_akses;?></td>
													<td style="width: 10%;">
														<div class="d-flex">
															<button class="btn btn-success btn-detail p-2 mr-2" value="<?php echo $key->id_user;?>">Detail</button>
															<button class="btn btn-warning btn-ubah p-2 mr-2" value="<?php echo $key->id_user;?>">Ubah</button>
															<?php if($key->id_user!=1){?>
																<button class="btn btn-danger btn-hapus p-2" value="<?php echo $key->id_user;?>">Hapus</button>
															<?php }?>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>
	
	<div class="modal fade modal-tampil" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"></h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<form class="form-horizontal form-label-left" method="POST" autocomplete="off">
					<div class="modal-body">
						<input name="id_user" type="hidden" class="id_user">
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nama Lengkap <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="nama_user" type="text" class="form-control nama_user" maxlength="30" placeholder="Masukkan Nama User" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Username <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="username" type="text" class="form-control username" old="" minlength="5" maxlength="20" placeholder="Masukkan Username" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Password <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="password" type="password" placeholder="Masukkan Password" minlength="8" maxlength="20" class="form-control password">
								<span class="text-password">*kosongkan jika tidak ingin mengubah password</span>
							</div>
						</div>
						<div class="form-group row  align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Hak Akses <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<select name="hak_akses" class="form-control hak_akses" required>
									<option value="">-Pilih Hak Akses-</option>
									<option value="Administrator">Administrator</option>
									<option value="Pemilik">Pemilik</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-dark btn-tutup" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary btn-simpan">Simpan</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<?php $this->load->view('back/partials/script');?>

	<script>
		$(".btn-tambah").on("click", function(){
			$("#myModalLabel").html('Tambah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'User/simpan');
			$(".password").prop("required", true);
			clear();
			$(".text-password").hide();
			$(".username").attr("old","");
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-detail", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Detail Data');
      		$("form").attr("action","");
			$(".password").prop("required", false);
			clear();
			get_data(id);
      		$("form :input:not('.btn-tutup')").prop("disabled",true);
			$(".text-password").hide();
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-ubah", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Ubah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'User/ubah');
			$(".password").prop("required", false);
			clear();
			get_data(id);
			$(".text-password").show();
			$(".modal-tampil").modal("show");
		});

		$(document).on("click",".btn-hapus", function(){
			var id = $(this).attr("value");
			hapus(id,'User');
		});

		$(".username").on("keyup", function(){
			var text = $(this).val();
			var username_old = $(this).attr("old");
			var type = $(this).attr("name");
			$.ajax({
				url : "<?php echo base_url();?>"+'User/validasi',
				type: "POST",
				data: {
					text:text,
					type:type
				},
				dataType: 'json',
				success: function(respond){
					if(respond==true && username_old!=text){
						notify('fa fa-times', 'danger', 'Peringatan', type+' sudah digunakan');
						$(".btn-simpan").prop("disabled",true);
					}else{
						$(".btn-simpan").prop("disabled",false);
					}
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		function get_data(id_user){
			$.ajax({
				url : "<?php echo base_url();?>"+'User/tampil_user',
				type: "POST",
				data: {
					id_user:id_user
				},
				dataType: 'json',
				success: function(respond){
					$(".id_user").val(respond.id_user);
					$(".nama_user").val(respond.nama_user);
					$(".username").val(respond.username);
					$(".username").attr("old",respond.username);
					$(".hak_akses").val(respond.hak_akses);
				},
				error: function() {
					console.log('Error');
				}
			});
		};
	</script>
</body>
</html>
