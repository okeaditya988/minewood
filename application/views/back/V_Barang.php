<!DOCTYPE html>
<html lang="en">

<head>

	<title>Barang - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Barang</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Barang</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Barang</div>
								<button class="btn btn-lg btn-primary p-2 btn-tambah" type="button"><i class="fa fa-plus"></i> Tambah</button>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Jenis Barang</th>
												<th>Nama Barang</th>
												<th>Foto Barang</th>
												<th>Harga Jual</th>
												<th>Stok Barang</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $key->jenis_barang;?></td>
													<td><?php echo $key->nama_barang;?></td>
													<td><img src="<?php echo base_url().'uploads/barang/'.$key->foto_barang;?>" style="max-width: 150px;"/></td>
													<td>Rp. <?php echo formatRupiah($key->harga_jual);?></td>
													<td><?php echo $key->stok;?></td>
													<td style="width: 10%;">
														<div class="d-flex">
															<button class="btn btn-warning btn-ubah p-2 mr-2" value="<?php echo $key->id_barang;?>">Ubah</button>
															<?php if($key->disabled==false){?>
																<button class="btn btn-danger btn-hapus p-2" value="<?php echo $key->id_barang;?>">Hapus</button>
															<?php }?>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>
	
    <div class="modal fade modal-tampil" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"></h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<form class="form-horizontal form-label-left" method="POST" autocomplete="off" enctype="multipart/form-data">
					<div class="modal-body">
						<input name="id_barang" type="hidden" class="id_barang">
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Jenis Barang <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<select name="id_jenis_barang" type="text" class="form-control id_jenis_barang" required>
									<option value="">-Pilih Jenis Barang-</option>
									<?php foreach($tampil_jenis_barang as $key){?>
										<option	value="<?php echo $key->id_jenis_barang;?>"><?php echo $key->jenis_barang;?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nama Barang <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="nama_barang" type="text" class="form-control nama_barang" maxlength="50" placeholder="Masukkan Nama Barang" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Stok Barang <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="stok" type="number" class="form-control stok" placeholder="Masukkan Stok Barang" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Foto Barang <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="foto_barang" type="file" class="form-control foto_barang" accept="image/*" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Berat Barang (gram) <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="berat" type="number" class="form-control berat" min="1" placeholder="Masukkan Berat Barang" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Harga Beli <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="harga_beli" type="text" class="form-control harga_beli harga" placeholder="Masukkan Harga Beli" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Harga Jual <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="harga_jual" type="text" class="form-control harga_jual harga" placeholder="Masukkan Harga Jual" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Deskripsi <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<textarea name="deskripsi" class="form-control deskripsi" old="" placeholder="Masukkan Deskripsi" required></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-danger btn-simpan">Simpan</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<?php $this->load->view('back/partials/script');?>

    <script>

		$(".stok").on("keyup", function(){
			var value = $(this).val();
			if(parseInt(value)<0){
				$(this).val("0")
			}
		});

		$(".btn-tambah").on("click", function(){
			$("#myModalLabel").html('Tambah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Barang/simpan');
			clear();
			$(".nama_barang").attr("old","");
			$(".foto_barang").prop("required",true);
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-ubah", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Ubah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Barang/ubah');
			clear();
			get_data(id);
			$(".foto_barang").prop("required",false);
			$(".modal-tampil").modal("show");
		});

		$(document).on("click",".btn-hapus", function(){
			var id = $(this).attr("value");
			hapus(id,'Barang');
		});

		$(".nama_barang").on("keyup", function(){
			var text = $(this).val();
			var nama_barang_old = $(this).attr("old");
			var select = $(".id_jenis_barang").children("option:selected").val();
			$.ajax({
				url : "<?php echo base_url();?>"+'Barang/validasi',
				type: "POST",
				data: {
					text:text,
					select:select
				},
				dataType: 'json',
				success: function(respond){
					if(respond==true && nama_barang_old!=text){
						notify('fa fa-times', 'danger', 'Peringatan', 'Nama Barang sudah digunakan');
						$(".btn-simpan").prop("disabled",true);
					}else{
						$(".btn-simpan").prop("disabled",false);
					}
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		function get_data(id_barang){
			$.ajax({
				url : "<?php echo base_url();?>"+'Barang/tampil_barang',
				type: "POST",
				data: {
					id_barang:id_barang
				},
				dataType: 'json',
				success: function(respond){
					$(".id_barang").val(respond.id_barang);
					$(".id_jenis_barang").val(respond.id_jenis_barang);
					$(".nama_barang").val(respond.nama_barang);
					$(".nama_barang").attr("old",respond.nama_barang);
					$(".stok").val(respond.stok);
					$(".berat").val(respond.berat);
					$(".harga_beli").val(formatRupiah(respond.harga_beli));
					$(".harga_jual").val(formatRupiah(respond.harga_jual));
					$(".deskripsi").val(respond.deskripsi);
				},
				error: function() {
				console.log('Error');
				}
			});
		};
	</script>
</body>
</html>
