<!DOCTYPE html>
<html lang="en">

<head>

	<title>Pembayaran - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Pembayaran</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Transaksi</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Pembayaran</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Pembayaran</div>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal Pesanan</th>
												<th>Tanggal Pembayaran</th>
												<th>Tipe Pembayaran</th>
												<th>Nama Pembeli</th>
												<th>Total Pesanan</th>
												<th>Status</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>	
													<td><?php echo longdate_indo($key->tanggal_pesanan).' Pukul '.time_indo($key->tanggal_pesanan);?></td>
													<td><?php if($key->tanggal_pembayaran!='0000-00-00 00:00:00' && $key->tanggal_pembayaran!=null){echo longdate_indo($key->tanggal_pembayaran).' Pukul '.time_indo($key->tanggal_pembayaran);}else{ echo '-';}?></td>
													<td><?php echo $key->tipe_pembayaran;?></td>
													<td><?php echo $key->nama;?></td>
													<td>Rp. <?php echo formatRupiah($key->total_harga);?></td>
													<td><?php echo $key->status_pesanan;?></td>
													<td style="width: 10%;">
														<div class="d-flex">
															<a target="_blank" href="<?php echo base_url().'Pesanan_Pembeli/detail?id='.$key->id_pesanan;?>"><button class="btn btn-warning p-2 mr-2">Detail Pesanan</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>

	<?php $this->load->view('back/partials/script');?>

</body>
</html>
