<!DOCTYPE html>
<html lang="en">

<head>

	<title>Pengiriman - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Pengiriman</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Transaksi</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Pengiriman</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Pengiriman</div>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal Pesanan</th>
												<th>Nomor Resi</th>
												<th>Tanggal Dikirm</th>
												<th>Tanggal Sampai</th>
												<th>Nama Pembeli</th>
												<th>Total Pesanan</th>
												<th>Status</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo longdate_indo($key->tanggal_pesanan).' Pukul '.time_indo($key->tanggal_pesanan);?></td>
													<td><?php echo $key->nomor_resi;?></td>
													<td><?php if($key->tanggal_kirim!='0000-00-00 00:00:00' && $key->tanggal_kirim!=null){echo longdate_indo($key->tanggal_kirim).' Pukul '.time_indo($key->tanggal_kirim);}else{ echo '-';}?></td>
													<td><?php if($key->tanggal_sampai!='0000-00-00 00:00:00' && $key->tanggal_kirim!=null){echo longdate_indo($key->tanggal_sampai).' Pukul '.time_indo($key->tanggal_sampai);}else{ echo '-';}?></td>
													<td><?php echo $key->nama;?></td>
													<td>Rp. <?php echo formatRupiah($key->total_harga);?></td>
													<td><?php echo $key->status_pengiriman;?></td>
													<td style="width: 10%;">
														<div class="d-flex">
															<a target="_blank" href="<?php echo base_url().'Pesanan_Pembeli/detail?id='.$key->id_pesanan;?>"><button class="btn btn-warning p-2 mr-2">Detail Pesanan</button></a>
															<button class="btn btn-primary p-2 mr-2 btn-kirim" value="<?php echo $key->id_pesanan;?>">Input Resi</button>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>

	<div class="modal fade modal-tampil" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"></h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<form class="form-horizontal form-label-left" method="POST" autocomplete="off">
					<div class="modal-body">
						<input name="id_pesanan" type="hidden" class="id_pesanan">
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nomor Resi <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="no_resi" type="text" class="form-control no_resi" maxlength="20" placeholder="Masukkan Nomor Resi" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-danger btn-simpan">Simpan</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<?php $this->load->view('back/partials/script');?>

	<script>
		$(document).on("click",".btn-kirim", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Kirim Pesanan');
      		$("form").attr("action","<?php echo base_url();?>"+'Pengiriman/ubah');
			clear();
			$(".id_pesanan").val(id);
			$(".modal-tampil").modal("show");
		});

	</script>

</body>
</html>
