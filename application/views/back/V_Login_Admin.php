<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Login - Minewood Dashboard</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/images/favicon.png">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
									<div class="text-center mb-3">
										<a href="<?php echo base_url();?>Login_Admin">
											<h2 class="text-white font-weight-bold">Minewood Admin</h2>
										</a>
										<hr>
									</div>
                                    <h4 class="text-center mb-4 text-white">Masuk Dashboard Admin</h4>
									<form action="<?php echo base_url();?>Login_Admin/auth" method="POST">
                                        <div class="form-group">
                                            <label class="mb-1 text-white"><strong>Username</strong></label>
                                            <input type="text" name="username" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-1 text-white"><strong>Password</strong></label>
                                            <input type="password" name="password" class="form-control" required>
                                        </div>
                                        <div class="text-center mt-5">
                                            <button type="submit" class="btn btn-secondary text-white btn-block">Masuk</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="<?php echo base_url();?>assets/vendor/global/global.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/deznav-init.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap-growl.min.js"></script>


	<script>
		function notify(icon, type, title, message){
			$.growl({
				icon: icon,
				title: title+' : ',
				message: message,
				url: ''
			},{
				element: 'body',
				type: type,
				allow_dismiss: true,
				placement: {
					from: 'top',
					align: 'right'
				},
				offset: {
					x: 30,
					y: 30
				},
				spacing: 10,
				z_index: 999999,
				delay: 2500,
				timer: 1000,
				url_target: '_blank',
				mouse_over: false,
				icon_type: 'class',
				template: '<div data-growl="container" class="alert" role="alert">' +
				'<span data-growl="icon" class="mr-1"></span>' +
				'<span data-growl="title"></span>' +
				'<span data-growl="message"></span>' +
				'<a href="#" data-growl="url"></a>' +
				'</div>'
			});
		};
	</script>

	<?php if($this->session->flashdata('sukses')){?>
		<script>
			notify('fa fa-check', 'success', 'Sukses', '<?php echo $this->session->flashdata('sukses');?>');
		</script>
	<?php }?>

	<?php if($this->session->flashdata('gagal')){?>
		<script>
			notify('fa fa-times', 'danger', 'Gagal', '<?php echo $this->session->flashdata('gagal');?>');
		</script>
	<?php }?>

</body>

</html>
