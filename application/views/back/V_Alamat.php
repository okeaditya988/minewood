<!DOCTYPE html>
<html lang="en">

<head>

	<title>Alamat - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<div class="d-flex justify-content-between">
						<h4>Alamat <?php echo $tampil_pembeli->nama?></h4>
						<a href="<?php echo base_url();?>Pembeli"><button class="btn btn-lg btn-outline-primary p-2" type="button"><i class="fa fa-arrow-left"></i> Kembali</button></a>
					</div>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url();?>Pembeli">Pembeli</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Alamat</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Alamat</div>
								<button class="btn btn-lg btn-primary p-2 btn-tambah" type="button"><i class="fa fa-plus"></i> Tambah</button>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Penerima</th>
												<th>Nomor Telepon</th>
												<th>Alamat</th>
												<th>Alamat Utama</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>	
													<td><?php echo $key->nama_penerima;?></td>
													<td><?php echo $key->nomor_telepon;?></td>
													<td><?php echo $key->alamat.', '.$key->nama_kecamatan.', '.$key->nama_kabupaten.', '.$key->nama_provinsi;?></td>
													<td><?php if($key->default==='1'){echo 'Alamat Utama';}else{echo '-';};?></td>
													<td style="width: 10%;">
														<div class="d-flex">
															<button class="btn btn-success btn-detail p-2 mr-2" value="<?php echo $key->id_alamat;?>">Detail</button>
															<button class="btn btn-warning btn-ubah p-2 mr-2" value="<?php echo $key->id_alamat;?>">Ubah</button>
															<?php if($key->disabled==false){?>
																<button class="btn btn-danger btn-hapus p-2" value="<?php echo $key->id_alamat;?>">Hapus</button>
															<?php }?>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>
	
	<div class="modal fade modal-tampil" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"></h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<form class="form-horizontal form-label-left" method="POST" autocomplete="off">
					<div class="modal-body">
						<input name="id_alamat" type="hidden" class="id_alamat">
						<input name="id_pembeli" type="hidden" class="id_pembeli" value="<?php echo $tampil_pembeli->id_pembeli?>">
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nama Penerima <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="nama_penerima" type="text" class="form-control nama_penerima" maxlength="30" placeholder="Masukkan Nama Penerima" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nomor Telepon <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="nomor_telepon" type="text" class="form-control nomor_telepon" old="" maxlength="16" placeholder="Masukkan Nomor Telepon" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Alamat <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<textarea name="alamat" class="form-control alamat" old="" minlength="5" maxlength="100" placeholder="Masukkan Alamat" required></textarea>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Provinsi <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input type="hidden" name="id_kabupaten_old" class="id_kabupaten_old">
								<select name="id_provinsi" type="text" class="form-control id_provinsi" required>
									<option value="">-Pilih Provinsi-</option>
									<?php foreach($tampil_provinsi as $key){?>
										<option	value="<?php echo $key->id_provinsi;?>"><?php echo $key->nama_provinsi;?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Kabupaten <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input type="hidden" name="id_kecamatan_old" class="id_kecamatan_old">
								<select name="id_kabupaten" type="text" class="form-control id_kabupaten" required>
									<option value="">-Pilih Kabupaten-</option>
								</select>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Kecamatan <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<select name="id_kecamatan" type="text" class="form-control id_kecamatan" required>
									<option value="">-Pilih Kecamatan-</option>
								</select>
							</div>
						</div>
						<div class="form-group row  align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Alamat Utama? <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<select name="default" class="form-control default" required>
									<option value="">-Pilih Alamat Utama-</option>
									<option value="1">Alamat Utama</option>
									<option value="0">Bukan Alamat Utama</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-dark btn-tutup" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary btn-simpan">Simpan</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<?php $this->load->view('back/partials/script');?>

	<script>
		$(".btn-tambah").on("click", function(){
			$("#myModalLabel").html('Tambah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Alamat/simpan');
			clear();
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-detail", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Detail Data');
      		$("form").attr("action","");
			clear();
			get_data(id);
      		$("form :input:not('.btn-tutup')").prop("disabled",true);
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-ubah", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Ubah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Alamat/ubah');
			clear();
			get_data(id);
			$(".modal-tampil").modal("show");
		});

		$(document).on("click",".btn-hapus", function(){
			var id = $(this).attr("value");
			hapus(id,'Alamat');
		});

		$(".id_provinsi").on("change", function(){
			var id_provinsi = $(this).children("option:selected").val();
			var id_kabupaten = $(".id_kabupaten_old").val();
			$(".id_kecamatan").val("");
			$(".id_kabupaten").val("");
			$.ajax({
				url : "<?php echo base_url();?>"+'Kabupaten/get_kabupaten',
				type: "POST",
				data: {
					id_provinsi:id_provinsi,
					id_kabupaten:id_kabupaten
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kabupaten").html(respond);
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		$(".id_kabupaten").on("change", function(){
			var id_kabupaten = $(".id_kabupaten").children("option:selected").val();
			if(!id_kabupaten){
				id_kabupaten = $(".id_kabupaten_old").val();
			}
			var id_kecamatan = $(".id_kecamatan_old").val();
			$.ajax({
				url : "<?php echo base_url();?>"+'Kecamatan/get_kecamatan',
				type: "POST",
				data: {
					id_kabupaten:id_kabupaten,
					id_kecamatan:id_kecamatan
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kecamatan").html(respond);
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		function get_data(id_alamat){
			$.ajax({
				url : "<?php echo base_url();?>"+'Alamat/tampil_alamat',
				type: "POST",
				data: {
					id_alamat:id_alamat
				},
				dataType: 'json',
				success: function(respond){
					$(".id_alamat").val(respond.id_alamat);
					$(".id_pembeli").val(respond.id_pembeli);
					$(".nama_penerima").val(respond.nama_penerima);
					$(".nomor_telepon").val(respond.nomor_telepon);
					$(".id_kecamatan_old").val(respond.id_kecamatan);
					$(".id_kabupaten_old").val(respond.id_kabupaten);
					$(".id_provinsi").val(respond.id_provinsi).change();
					$(".id_kabupaten").change();
					$(".alamat").val(respond.alamat);
					$(".default").val(respond.default);
				},
				error: function() {
					console.log('Error');
				}
			});
		};
	</script>
</body>
</html>
