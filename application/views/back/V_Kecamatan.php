<!DOCTYPE html>
<html lang="en">

<head>

	<title>Kecamatan - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Kecamatan</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Kecamatan</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Kecamatan</div>
								<button class="btn btn-lg btn-primary p-2 btn-tambah" type="button"><i class="fa fa-plus"></i> Tambah</button>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Provinsi</th>
												<th>Nama Kabupaten</th>
												<th>Nama Kecamatan</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $key->nama_provinsi;?></td>
													<td><?php echo $key->nama_kabupaten;?></td>
													<td><?php echo $key->nama_kecamatan;?></td>
													<td style="width: 10%;">
														<div class="d-flex">
															<button class="btn btn-warning btn-ubah p-2 mr-2" value="<?php echo $key->id_kecamatan;?>">Ubah</button>
															<?php if($key->disabled==false){?>
																<button class="btn btn-danger btn-hapus p-2" value="<?php echo $key->id_kecamatan;?>">Hapus</button>
															<?php }?>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>
	
    <div class="modal fade modal-tampil" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"></h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<form class="form-horizontal form-label-left" method="POST" autocomplete="off">
					<div class="modal-body">
						<input name="id_kecamatan" type="hidden" class="id_kecamatan">
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nama Provinsi <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input type="hidden" name="id_kabupaten_old" class="id_kabupaten_old">
								<select name="id_provinsi" type="text" class="form-control id_provinsi" required>
									<option value="">-Pilih Provinsi-</option>
									<?php foreach($tampil_provinsi as $key){?>
										<option	value="<?php echo $key->id_provinsi;?>"><?php echo $key->nama_provinsi;?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nama Kabupaten <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<select name="id_kabupaten" type="text" class="form-control id_kabupaten" required>
									<option value="">-Pilih Kabupaten-</option>
								</select>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nama Kecamatan <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="nama_kecamatan" type="text" class="form-control nama_kecamatan" maxlength="50" placeholder="Masukkan Nama Kecamatan" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-danger btn-simpan">Simpan</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<?php $this->load->view('back/partials/script');?>

    <script>
		$(".btn-tambah").on("click", function(){
			$("#myModalLabel").html('Tambah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Kecamatan/simpan');
			clear();
			$(".nama_kecamatan").attr("old","");
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-ubah", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Ubah Data');
      		$("form").attr("action","<?php echo base_url();?>"+'Kecamatan/ubah');
			clear();
			get_data(id);
			$(".modal-tampil").modal("show");
		});

		$(document).on("click",".btn-hapus", function(){
			var id = $(this).attr("value");
			hapus(id,'Kecamatan');
		});

		$(".id_provinsi").on("change", function(){
			var id_provinsi = $(this).children("option:selected").val();
			var id_kabupaten = $(".id_kabupaten_old").val();
			$.ajax({
				url : "<?php echo base_url();?>"+'Kabupaten/get_kabupaten',
				type: "POST",
				data: {
					id_provinsi:id_provinsi,
					id_kabupaten:id_kabupaten
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kabupaten").html(respond);
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		$(".nama_kecamatan").on("keyup", function(){
			var text = $(this).val();
			var nama_kecamatan_old = $(this).attr("old");
			var select = $(".id_provinsi").children("option:selected").val();
			$.ajax({
				url : "<?php echo base_url();?>"+'Kecamatan/validasi',
				type: "POST",
				data: {
					text:text,
					select:select
				},
				dataType: 'json',
				success: function(respond){
					if(respond==true && nama_kecamatan_old!=text){
						notify('fa fa-times', 'danger', 'Peringatan', 'Nama Kecamatan sudah digunakan');
						$(".btn-simpan").prop("disabled",true);
					}else{
						$(".btn-simpan").prop("disabled",false);
					}
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		function get_data(id_kecamatan){
			$.ajax({
				url : "<?php echo base_url();?>"+'Kecamatan/tampil_kecamatan',
				type: "POST",
				data: {
					id_kecamatan:id_kecamatan
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kabupaten_old").val(respond.id_kabupaten);
					$(".id_provinsi").val(respond.id_provinsi).change();
					$(".id_kecamatan").val(respond.id_kecamatan);
					$(".nama_kecamatan").val(respond.nama_kecamatan);
					$(".nama_kecamatan").attr("old",respond.nama_kecamatan);
				},
				error: function() {
				console.log('Error');
				}
			});
		};
	</script>
</body>
</html>
