<!DOCTYPE html>
<html lang="en">

<head>

	<title>Laporan Pesanan - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Laporan Pesanan</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Laporan Pesanan</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Laporan Pesanan</div>
							</div>
                        </div>
                    </div>
					<div class="col-12">
						<div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Cetak Laporan</div>
							</div>
							<div class="card-body">
								<form action="<?php echo base_url();?>Laporan_Pesanan/cetak" method="POST" autocomplete="off">
									<div class="row align-items-center">
										<div class="col-md-4 col-sm-4 penjualan">
											<label class="control-label">Status</label>
										</div>
										<div class="col-md-3 col-sm-3">
											<label class="control-label">Tanggal Mulai</label>
										</div>
										<div class="col-md-3 col-sm-3">
											<label class="control-label">Tanggal Selesai</label>
										</div>
										<div class="col-md-1 col-sm-1">
										</div>
										<div class="col-md-4 col-sm-4 penjualan">
											<select name="status" class="form-control status">
												<option value="">-Pilih Status-</option>
												<option value="Semua">Semua Status</option>
												<option value="Menunggu Pembayaran">Menunggu Pembayaran</option>
												<option value="Menunggu Dikirim">Menunggu Dikirim</option>
												<option value="Dalam Pengiriman">Dalam Pengiriman</option>
												<option value="Selesai">Selesai</option>
												<option value="Batal">Batal</option>
											</select>
										</div>
										<div class="col-md-3 col-sm-3">
											<input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" required>
										</div>
										<div class="col-md-3 col-sm-3">
											<input type="text" id="tanggal_selesai" name="tanggal_selesai" class="form-control" required>
										</div>
										<div class="col-md-1 col-sm-1">
											<button type="submit" class="btn btn-primary">Proses</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>
	
	<?php $this->load->view('back/partials/script');?>

	<script>

		$("#tanggal_mulai").datepicker( {
			format: "yyyy-mm-dd"
		});
		$("#tanggal_selesai").datepicker( {
			format: "yyyy-mm-dd"
		});

		$("#tanggal_mulai").on('changeDate', function(selected) {
			var startDate = new Date(selected.date.valueOf());
			$("#tanggal_selesai").datepicker('setStartDate', startDate);
			if($("#tanggal_mulai").val() > $("#tanggal_selesai").val()){
			$("#tanggal_selesai").val($("#tanggal_mulai").val());
			}
		});
	</script>

</body>
</html>
