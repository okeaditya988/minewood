<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/images/favicon.png">
<link href="<?php echo base_url();?>assets/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">

<link rel="stylesheet"href="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"href="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.css">
<link rel="stylesheet"href="<?php echo base_url();?>assets/icons/font-awesome-old/css/all.min.css">

<style>
	.form-control {
		color: #000 !important;
	}
</style>
