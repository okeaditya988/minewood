<script src="<?php echo base_url();?>assets/vendor/global/global.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/deznav-init.js"></script>

<!-- Datatable -->
<script src="<?php echo base_url();?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins-init/datatables.init.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-growl.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>


<script>
	$(".tanggal").datepicker( {
		format: "yyyy-mm-dd"
	});

	$(".tanggals").datepicker( {
		format: "yyyy-mm",
		viewMode: "months", 
		minViewMode: "months"
	});

	$(document).on("keyup",".harga",function() {
		$(this).val(formatRupiah($(this).val()));
	});

	function rupiahtoFloat(rupiah){
		return rupiah.split('.').join('');
	}

	function formatRupiah(angka, prefix){
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split   		= number_string.split(','),
		sisa     		= split[0].length % 3,
		rupiah     		= split[0].substr(0, sisa),
		ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

		if(ribuan){
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}

	function huruf(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
			return false;
		return true;
	}

	function angka(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode < 48 || charCode > 57)&&charCode>32)
			return false;
		return true;
	}

	function clear(){
		$('form').trigger("reset");
		$('form input, form select, form textarea, form button').prop("disabled",false);
		$(".kelompok-detail:not(:first)").remove();
	}

	function hapus(id,tabel){
		Swal.fire({
			html:
				'<p class="font-weight-bold mt-3" style="font-family: Roboto, sans-serif;font-size: 16px;"><b>Apakah Anda yakin ingin menghapus Data ini?</b></p><br><p class="font-weight-regular mb-1 px-3" style="font-family: Roboto, sans-serif; font-size: 16px;color: #7C8CA3!important " text-align="justify">Data yang telah dihapus tidak akan bisa dipulihkan kembali.</p>',
			showCancelButton: true,
			cancelButtonText: "Batal",
			confirmButtonText: 'Hapus',
			confirmButtonColor: '#dc3545',
			reverseButtons: true
		}).then((result) => {
			if(result.value){
				$.ajax({
					url : "<?php echo base_url();?>"+tabel+"/hapus",
					type : "POST",
					data : {
						id : id
					},
					success : function(data) {
                        notify('fa fa-check', 'success', 'Sukses', 'Berhasil Dihapus');
						setTimeout(
							function() {
								location.reload();
							}, 1000
						);
					},
					error : function () {
                        notify('fa fa-times', 'danger', 'Gagal', 'Gagal Dihapus');
						setTimeout(
							function() {
								location.reload();
							}, 1000
						);
					}
				});
			}
		});
	}

    function notify(icon, type, title, message){
        $.growl({
            icon: icon,
            title: title+' : ',
            message: message,
            url: ''
        },{
            element: 'body',
            type: type,
            allow_dismiss: true,
            placement: {
                from: 'top',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            spacing: 10,
            z_index: 999999,
            delay: 2500,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            icon_type: 'class',
            template: '<div data-growl="container" class="alert" role="alert">' +
            '<span data-growl="icon" class="mr-1"></span>' +
            '<span data-growl="title"></span>' +
            '<span data-growl="message"></span>' +
            '<a href="#" data-growl="url"></a>' +
            '</div>'
        });
    };
</script>

<?php if($this->session->flashdata('sukses')){?>
	<script>
        notify('fa fa-check', 'success', 'Sukses', '<?php echo $this->session->flashdata('sukses');?>');
	</script>
<?php }?>

<?php if($this->session->flashdata('gagal')){?>
	<script>
        notify('fa fa-times', 'danger', 'Gagal', '<?php echo $this->session->flashdata('gagal');?>');
	</script>
<?php }?>
