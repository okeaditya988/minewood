<div class="deznav">
	<div class="deznav-scroll">
		<ul class="metismenu" id="menu">
			<li><a href="<?php echo base_url();?>Dashboard" class="ai-icon" aria-expanded="false">
					<i class="flaticon-381-networking"></i>
					<span class="nav-text">Dashboard</span>
				</a>
			</li>
			<li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
					<i class="flaticon-381-television"></i>
					<span class="nav-text">Master</span>
				</a>
				<ul aria-expanded="false">
					<li><a href="<?php echo base_url();?>Jenis_Barang">Jenis Barang</a></li>
					<li><a href="<?php echo base_url();?>Barang">Barang</a></li>
					<li><a href="<?php echo base_url();?>User">User</a></li>
					<li><a href="<?php echo base_url();?>Pembeli">Pembeli</a></li>
					<li><a href="<?php echo base_url();?>Provinsi">Provinsi</a></li>
					<li><a href="<?php echo base_url();?>Kabupaten">Kabupaten</a></li>
					<li><a href="<?php echo base_url();?>Kecamatan">Kecamatan</a></li>
				</ul>
			</li>
			<li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
					<i class="flaticon-381-gift"></i>
					<span class="nav-text">Transaksi</span>
				</a>
				<ul aria-expanded="false">
					<li><a href="<?php echo base_url();?>Pesanan">Pemesanan</a></li>
					<li><a href="<?php echo base_url();?>Pembayaran">Pembayaran</a></li>
					<li><a href="<?php echo base_url();?>Pengiriman">Pengiriman</a></li>
				</ul>
			</li>
			<li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
					<i class="flaticon-381-layer-1"></i>
					<span class="nav-text">Laporan</span>
				</a>
				<ul aria-expanded="false">
					<li><a href="<?php echo base_url();?>Laporan_Pesanan">Pemesanan</a></li>
					<li><a href="<?php echo base_url();?>Laporan_Pembayaran">Pembayaran</a></li>
					<li><a href="<?php echo base_url();?>Laporan_Pengiriman">Pengiriman</a></li>
				</ul>
			</li>
		</ul>
		
		<div class="copyright">
			<p><strong>Minewood Dashboard</strong> © 2022 All Rights Reserved</p>
			<p>by Zada</p>
		</div>
	</div>
</div>
