<!DOCTYPE html>
<html lang="en">

<head>

	<title>Pesanan - Minewood Admin</title>

	<?php $this->load->view('back/partials/stylesheet');?>
	
</head>

<body>

	<?php $this->load->view('back/partials/loader');?>

    <div id="main-wrapper">

		<?php $this->load->view('back/partials/header');?>

		<?php $this->load->view('back/partials/menu');?>
        
        <div class="content-body">
            <div class="container-fluid">
                <div class="page-titles">
					<h4>Pesanan</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Transaksi</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Pesanan</a></li>
					</ol>
                </div>
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header d-flex align-items-center justify-content-between">
								<div>Data Pesanan</div>
							</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal Pesanan</th>
												<th>Nama Pembeli</th>
												<th>Alamat</th>
												<th>Total Pesanan</th>
												<th>Status</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($tampil as $key){?>
												<tr>
													<td><?php echo $no;?></td>	
													<td><?php echo longdate_indo($key->tanggal_pesanan).' Pukul '.time_indo($key->tanggal_pesanan);?></td>
													<td><?php echo $key->nama;?></td>
													<td><?php echo $key->nama_kecamatan.', '.$key->nama_kabupaten.', '.$key->nama_provinsi;?></td>
													<td>Rp. <?php echo formatRupiah($key->total_harga);?></td>
													<td><?php echo $key->status_pesanan;?></td>
													<td style="width: 10%;">
														<div class="d-flex justify-content-center">
															<?php if($key->status_pesanan=='Menunggu Pembayaran'){?>
																<button class="btn btn-danger btn-dibatalkan" id_pesanan="<?php echo $key->id_pesanan?>">Batalkan Pesanan</button>
															<?php }?>
															<a target="_blank" href="<?php echo base_url().'Pesanan_Pembeli/detail?id='.$key->id_pesanan;?>"><button class="btn btn-warning ml-2">Detail Pesanan</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; }?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
		
		<?php $this->load->view('back/partials/footer');?>
        
    </div>

	<?php $this->load->view('back/partials/script');?>

	<script>
		$(".btn-dibatalkan").on("click", function(){
			var id_pesanan = $(this).attr("id_pesanan");
			Swal.fire({
				html:
					'<p class="font-weight-bold mt-3" style="font-family: Roboto, sans-serif;font-size: 16px;"><b>Apakah Anda yakin akan membatalkan pesanan ini?</b></p><br><p class="font-weight-regular mb-1 px-3" style="font-family: Roboto, sans-serif; font-size: 16px;color: #7C8CA3!important " text-align="justify">Pesanan yang dibatalkan tidak dapat digunakan kembali.</p>',
				showCancelButton: true,
				cancelButtonText: "Batal",
				confirmButtonText: 'Yakin',
				confirmButtonColor: '#dc3545',
				reverseButtons: true
			}).then((result) => {
				if(result.value){
					$.ajax({
						url : "<?php echo base_url();?>Pesanan/batalkan",
						type : "POST",
						data : {
							id : id_pesanan
						},
						success : function(data) {
							notify('fa fa-check', 'success', 'Sukses', 'Berhasil Dibatalkan');
							setTimeout(
								function() {
									location.reload();
								}, 1000
							);
						},
						error : function () {
							notify('fa fa-times', 'danger', 'Gagal', 'Gagal Dibatalkan');
							setTimeout(
								function() {
									location.reload();
								}, 1000
							);
						}
					});
				}
			});
		});
	</script>

</body>
</html>
