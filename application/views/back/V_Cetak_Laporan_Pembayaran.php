<table border="0" cellpadding="1" cellspacing="1" style="width:680px">
  <tbody>
    <tr>
      <td style="text-align:center">
        <!-- <img src="<?= base_url(); ?>/assets/media/logo/logo.png" width="height:90px; width:90px"> -->
        <!-- <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/ukm-cimanggu/assets/media/logo/logo.png" width="height:90px; width:90px"> -->
      </td>
      <td>
        <p style="text-align:center"><span style="font-family:Times New Roman,Times,serif">
            <font size="6">MINEWOOD STUDIO</font>
          </span></p>
        <p style="text-align:center"><span style="font-size:14px">Jl. Masjid No.8, Kauman, Kowangan, Temanggung 56218
            Telp. (0351) 7668010 </span></p>
      </td>
    </tr>
  </tbody>
</table>

<hr />
<p>&nbsp;</p>

<p style="text-align:center"><strong><span style="font-size:14px">LAPORAN TRANSAKSI PEMBAYARAN</span></strong></p>
<p style="text-align:left"><span style="font-size:12px"><strong>Tanggal : <?php echo $tanggal;?></strong></span></p>
<br>
<table style="font-size:12px" width="520" cellpadding="0" cellspacing="0" border="1">
  <thead>
    <tr style="text-align:center">
      <th>No</th>
      <th>Tanggal Pesanan</th>
      <th>Tanggal Pembayaran</th>
      <th>Metode Pembayaran</th>
      <th>Nama Pembeli</th>
      <th>Total Harga</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $no = 1;?>
    <?php foreach ($laporan as $key) {
			if($key->nama){
          ?>
    <tr>
      <td>&nbsp;<?php echo $no;?></td>
      <td>&nbsp;<?php echo longdate_indo($key->tanggal_pesanan);?></td>
      <td>&nbsp;<?php if($key->tanggal_pembayaran!='0000-00-00 00:00:00' && $key->tanggal_pembayaran!=null){echo longdate_indo($key->tanggal_pembayaran).' Pukul '.time_indo($key->tanggal_pembayaran);}else{ echo '-';}?></td>
      <td>&nbsp;<?php echo $key->tipe_pembayaran;?></td>
      <td>&nbsp;<?php echo $key->nama;?></td>
      <td>&nbsp;Rp <?php echo formatRupiah($key->total_pesanan);?></td>
      <td style="text-align: center;">&nbsp;<?php echo strtoupper($key->status_pesanan)?></td>
    </tr>
    <?php
          $no++;
				}
      }
    ?>
  </tbody>
</table>

<p>&nbsp;</p>



<span style="margin-left:450px;font-size:12px">Mengetahui,</span>
<span style="margin-left:450px;font-size:12px">Temanggung, <?php echo $tanggal_cetak;?></span>
<br><br><br><br>
<span style="margin-left:450px;font-size:12px"><?php echo $this->session->userdata('nama_user');?></span>

<p style="margin-left:450px">&nbsp;</p>

<span style="margin-left:450px;font-size:12px"></span>
