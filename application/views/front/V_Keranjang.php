<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Keranjang</title>

	<?php $this->load->view('front/partials/stylesheet');?>

	<link href="<?php echo base_url();?>assets/front/css/checkout.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">

	<style>
		.card {
			position: relative;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-orient: vertical;
			-webkit-box-direction: normal;
			-ms-flex-direction: column;
			flex-direction: column;
			min-width: 0;
			word-wrap: break-word;
			background-color: #fff;
			background-clip: border-box;
			border: 1px solid rgba(0,0,0,.125);
			border-radius: 0.25rem;
			margin-bottom: 20px;
		}
		.card-header:first-child {
			border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
		}
		.card-header {
			padding: 0.75rem 1.25rem;
			margin-bottom: 0;
			background-color: rgba(0,0,0,.03);
			border-bottom: 1px solid rgba(0,0,0,.125);
		}
		.card-body {
			-webkit-box-flex: 1;
			-ms-flex: 1 1 auto;
			flex: 1 1 auto;
			padding: 1.25rem;
		}

		.btn-dark {
			background-color: #d6d6d6;
			border: 1px solid #000;
			border-radius: 5px;
			color: #000;
		}
		.btn-dark:hover {
			background-color: #e8e8e8;
		}

		.d-flex {
			display: flex;
		}

		.justify-content-between {
			justify-content: space-between;
		}
	</style>

	<script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="SB-Mid-client-I56wk0hQ8a40nxZk"></script>
</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
			<ul class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i></a></li>
				<li><a href="<?php echo base_url();?>">Keranjang</a></li>
			</ul>

			<div class="row">
				<!--Middle Part Start-->
				<div id="content" class="col-sm-12">
					<form id="target" action="<?php echo base_url();?>Keranjang/perbarui" method="post">
					<div class="d-flex justify-content-between" style="margin-bottom: 20px;">
						<h2 class="title" style="margin-top: 0;margin-bottom: 0;">Keranjang</h2>
					</div>
						<div class="table-responsive form-group">
							<table class="table table-bordered">
								<thead>
									<tr>
										<td class="text-center">Gambar Produk</td>
										<td class="text-left">Nama Produk</td>
										<td class="text-left">Jumlah</td>
										<td class="text-right">Harga</td>
										<td class="text-right">Sub Total</td>
									</tr>
								</thead>
								<tbody>
									<?php if($tampil_detail){foreach($tampil_detail as $key){?>
										<tr>
											<input type="hidden" name="id_barang[]" value="<?php echo $key->id_barang?>">
											<td class="text-center"><a href="<?php echo base_url().'Detail_Produk?id='.$key->id_barang?>"><img width="70px"
														src="<?php echo base_url().'uploads/barang/'.$key->foto_barang?>" alt="<?php echo $key->nama_barang?>"
														title="<?php echo $key->nama_barang?>" class="img-thumbnail" /></a></td>
											</td>
											<td class="text-left"><?php echo $key->nama_barang?></td>
											<td class="text-left" width="200px">
												<div class="input-group btn-block quantity">
													<input type="text" name="jumlah_barang[]" value="<?php echo $key->jumlah?>" size="1"
														min="1" max="<?php echo $key->stok?>" class="form-control jumlah_barang" />
													<span class="input-group-btn">
														<button type="button" data-toggle="tooltip" title="Remove"
															class="btn btn-danger cart_quantity_delete" onClick=""><i
																class="fa fa-times-circle"></i></button>
													</span></div>
											</td>
											<td class="text-right">Rp. <?php echo formatRupiah($key->harga_jual)?></td>
											<td class="text-right">Rp. <?php echo formatRupiah($key->sub_total)?></td>
										</tr>
									<?php }}else{ ?>
										<tr>
											<td colspan="5" class="text-center">Belum Ada Produk di Keranjang</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
						</div>
    				</form>
						
					<div class="row">
						<div class="col-sm-6">
							<div class="card">
								<div class="card-header">
									<h3 class="subtitle no-margin">Pilih Alamat</h3>
								</div>
								<div class="card-body">
									<?php echo $tampil_alamat->alamat.', '.$tampil_alamat->nama_kecamatan.', '.$tampil_alamat->nama_kabupaten.', '.$tampil_alamat->nama_provinsi?>
									<a class="btn btn-sm btn-danger" style="margin-left: 10px;" href="<?php echo base_url();?>Profil">Ganti Alamat</a>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="card">
								<div class="card-header">
									<h3 class="subtitle no-margin">Pilih Jasa Pengiriman</h3>
								</div>
								<div class="card-body">
									<select class="kode_kurir" <?php if(!$tampil_detail){echo "disabled";}?> style="width: 100%;">
										<option value="">-Pilih Jasa Pengiriman-</option>
										<?php foreach($ekspedisi as $key){?>
											<option value="<?php echo $key['kode_layanan_ekspedisi'].':'.$key['kode_jenis_layanan']?>" harga="<?php echo $key['harga']?>"><?php echo $key['layanan_ekspedisi'].'-'.$key['kode_jenis_layanan'].' ('.$key['estimasi'].' Hari)'?></option>
										<?php }?>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4 col-sm-offset-8">
							<table class="table table-bordered">
								<tbody class="total_area">
									<input type="hidden" name="biaya_pengiriman" class="biaya_pengiriman_input" value="0">
									<input type="hidden" name="berat_total" class="berat_total_input" value="<?php if($tampil_detail){echo $tampil->total_berat;}?>">
									<tr>
										<td class="text-right">
											<strong>Total Belanja:</strong>
										</td>
										<td class="text-right sub_total" value="<?php if($tampil_detail){echo $tampil->total_harga;}?>">Rp. <?php if($tampil_detail){echo formatRupiah($tampil->total_harga);}else{echo '0';}?></td>
									</tr>
									<tr>
										<td class="text-right">
											<strong>Biaya Pengiriman:</strong>
										</td>
										<td class="text-right biaya_pengiriman">Rp. 0</td>
									</tr>
									<tr>
										<td class="text-right">
											<strong>Total:</strong>
										</td>
										<td class="text-right grand_total" value="<?php if($tampil_detail){echo $tampil->total_pesanan;}?>">Rp. <?php if($tampil_detail){echo formatRupiah($tampil->total_pesanan);}else{echo '0';}?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="buttons">
						<div class="pull-left"><a href="<?php echo base_url();?>" class="btn btn-dark">Lanjutkan Belanja</a></div>
						<div class="pull-right"><button type="button" class="btn btn-primary check_out" <?php if(!$tampil_detail){echo "disabled";}?>>Pembayaran</button></div>
					</div>
				</div>

			</div>
		</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

    <form id="payment-form" method="post" action="<?php echo base_url();?>Keranjang/simpan_pembayaran">
        <input type="hidden" name="result_type" id="result-type" value=""></div>
        <input type="hidden" name="result_data" id="result-data" value=""></div>
        <input type="hidden" name="id_pesanan" id="id-pesanan" value=""></div>
    </form>

	<?php $this->load->view('front/partials/script');?>

    <script src="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

	<script>
		$(".jumlah_barang").on("change", function(){
			var element = $(this);
			var value = element.val();
			var max = element.attr("max");
			
			value = parseInt(value);
			max = parseInt(max);
			if(value>max){
				element.val((max));
			}
			checkout();
		});

		$(".cart_quantity_delete").on("click", function(){
			$(this).closest("tr").remove();
			checkout();
		});

		$(".kode_kurir").on("change", function(){
            var harga = $(this).children("option:selected").attr('harga');
            var berat_total_input = $(".berat_total_input").val();
            var grand_total = $(".grand_total").attr("value");

            berat_total_input = Math.ceil(parseFloat(berat_total_input)/1000);
            harga = berat_total_input*parseFloat(harga);
            
            var total = parseFloat(grand_total)+parseFloat(harga);
            
            $(".biaya_pengiriman_input").val(harga);
            $(".biaya_pengiriman").html(berat_total_input+" Kg | "+"Rp. "+formatRupiah(harga.toString()));
            $(".grand_total").html("Rp. "+formatRupiah(total.toString()));
        });

		$(".check_out").on("click",function(){
            var kode_kurir = $(".kode_kurir").children("option:selected").val();
            var sub_total = $(".sub_total").attr("value");
            var biaya_pengiriman = $(".biaya_pengiriman_input").val();

            if(kode_kurir && sub_total!=0){
                Swal.fire({
                    html: '<p class="font-weight-bold mt-3" style="font-family: Roboto, sans-serif;font-size:20px;"><b>Apakah anda yakin melakukan checkout?</b></p><br><p class="font-weight-regular mb-1 px-3 text-center" style="font-family: Roboto, sans-serif; font-size: 16px;color: #7C8CA3!important " align="left">Pastikan kembali produk yang akan anda checkout sudah benar.</p>',
                    showCancelButton: true,
                    confirmButtonColor: '#34b4c7',
                    cancelButtonText: "Batalkan",
                    confirmButtonText: 'Ya, Lanjutkan!',
                    reverseButtons: true
                }).then((result) => {
                    if(result.value){
                        var sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';
                        $.ajax({
                            url : "<?php echo base_url();?>Keranjang/checkout",
                            type : "POST",
                            data : {
                                'kode_kurir' : kode_kurir,
                                'biaya_pengiriman' : biaya_pengiriman,
                            },
                            dataType: 'json',
                            beforeSend: function(){
                                $('.btn-checkout').prop("disabled",true);
                                swal.fire({
                                    html: '<h5>Loading...</h5>',
                                    showConfirmButton: false,
                                    onRender: function() {
                                        $('.swal2-content').prepend(sweet_loader);
                                    }
                                });
                            },
                            success : function(respond) {
                                var data = respond.token;

                                var resultType = document.getElementById('result-type');
                                var resultData = document.getElementById('result-data');

                                function changeResult(type,data){
                                    $("#result-type").val(type);
                                    $("#id-pesanan").val(respond.id_pesanan);
                                    $("#result-data").val(JSON.stringify(data));
                                }

                                snap.pay(data, {
                                    onSuccess: function(result){
                                        changeResult('success', result);
                                        $("#payment-form").submit();
                                    },
                                    onPending: function(result){
                                        changeResult('pending', result);
                                        $("#payment-form").submit();
                                    },
                                    onError: function(result){
                                        changeResult('error', result);
                                        $("#payment-form").submit();
                                    },
									onClose: function(){
										$.ajax({
											url : "<?php echo base_url();?>"+'Keranjang/batal',
											type: "POST",
											dataType: 'json',
											success: function(respond){
												console.log('Success');
												location.reload();
											},
											error: function() {
												console.log('Error');
												location.reload();
											}
										});
									}
                                });
                            },
                            error : function () {
                                error_timeout("Gagal Melakukan Checkout! Muat ulang kembali halaman!");
                            }
                        });
                    }
                });
            }
        });

		function checkout(){
			setTimeout(function () {
				$( "#target" ).submit();
			}, 500);
		}

        function error_timeout(msg){
            swal.fire({
                icon: 'error',
                html: '<h4>Upps!</h4><h5>'+msg+'</h5>',
            }).then((result) => {
                document.location.reload(true)
            });
        }

        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }
	</script>

</body>

</html>
