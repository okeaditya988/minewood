<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Detail Produk</title>

	<?php $this->load->view('front/partials/stylesheet');?>

	<style>
		.left-content-product .content-product-right .product-box-desc {
			margin: 50px 0;
		}
		.left-content-product .content-product-right .product-box-desc .inner-box-desc > div {
			margin-bottom: 10px;
		}
		.left-content-product .content-product-right .box-info-product .cart {
			margin-left: 110px;
			text-align: center;
		}
		.left-content-product .content-product-right .box-info-product .cart button {
			width: 200px !important;
		}
		.left-content-product .content-product-right .box-info-product .quantity .quantity-control {
			width: 150px !important;
		}
		.left-content-product .content-product-right .box-info-product .quantity .quantity-control .form-control {
			margin-left: 15px !important;
			width: 50px !important; 
			text-align: center;
		}
		.left-content-product .content-product-right .product-label .stock .status-nostock {
			color: #e74c3c;
		}
		.left-content-product .content-product-right .product-label .stock .status-nostock:before {
			content: "\f00d";
			font-family: FontAwesome;
			display: inline-block;
			color: #e74c3c;
			margin-right: 5px;
			margin-left: 10px;
		}
		.large-image .product-image-zoom {
			max-height: 360px;
		}

		.addToCart {
			position: relative;
			border: 1px solid #ddd;
			background: transparent;
			color: #fff;
			font-size: 12px;
			font-weight: bold;
			text-transform: uppercase;
			padding: 7px 15px;
			display: inline-block;
			background: #F4A137;
		}
		.addToCart i {
			margin-right: 5px;
		}
		.addToCart:hover {
			background: #F4A137;
			color: #444;
			border-color: #F4A137;
		}
		.addToCart:hover:before {
			background-position: 0px 0px;
		}

		@media only screen and (max-width: 600px) {
			.left-content-product .content-product-right .box-info-product .quantity .quantity-control {
				width: 130px !important;
			}
			.left-content-product .content-product-right .box-info-product .cart {
				margin-left: 5px;
				text-align: center;
			}
			.left-content-product .content-product-right .box-info-product .cart button {
				width: 180px !important;
			}
		}
	</style>

</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
				<li><a href="<?php echo base_url().'Kategori?id='.$produk->id_jenis_barang;?>"><?php echo $produk->jenis_barang?></a></li>
				<li><a href="#"><?php echo $produk->nama_barang?></a></li>

			</ul>

			<div class="row">
				<!--Middle Part Start-->
				<div id="content" class="col-md-12 col-sm-12">

					<div class="product-view row">
						<div class="left-content-product col-lg-10 col-xs-12">
							<div class="row">
								<div class="content-product-left class-honizol col-sm-6 col-xs-12 ">
									<div class="large-image text-center">
										<img itemprop="image" class="product-image-zoom"
											src="<?php echo base_url().'uploads/barang/'.$produk->foto_barang;?>"
											data-zoom-image="<?php echo base_url().'uploads/barang/'.$produk->foto_barang;?>" title="<?php echo $produk->nama_barang?>"
											alt="<?php echo $produk->nama_barang?>">
									</div>
								</div>

								<div class="content-product-right col-sm-6 col-xs-12">
									<div class="title-product">
										<h1><?php echo $produk->nama_barang?></h1>
									</div>
									
									<div class="product-label form-group">
										<div class="product_page_price price" itemprop="offerDetails" itemscope="">
											<span class="price-new" itemprop="price">Rp. <?php echo formatRupiah($produk->harga_jual)?></span>
										</div>
										<div class="stock"><span>Ketersedian:</span> <span class="status-<?php echo $produk->stok>0?"stock":"nostock"?>"><?php echo $produk->stok>0?"Tersedia":"Tidak Tersedia"?></span></div>
									</div>

									<div class="product-box-desc">
										<div class="inner-box-desc">
											<div class="brand"><span>Kategori:</span><a href="<?php echo base_url().'Kategori?id='.$produk->id_jenis_barang;?>"><?php echo $produk->jenis_barang?></a> </div>
											<div class="model"><span>Stok:</span> <?php echo $produk->stok?></div>
											<div class="reward"><span>Berat:</span> <?php echo $produk->berat?> gram</div>
										</div>
									</div>

									<div id="product">

										<div class="form-group box-info-product">
											<form action="<?php echo base_url();?>Keranjang/tambah" method="get">
												<div class="option quantity">
													<div class="input-group quantity-control" unselectable="on"
														style="-webkit-user-select: none;">
														<label>Qty</label>
														<input type="hidden" name="id" value="<?php echo $produk->id_barang;?>">
														<input class="form-control" type="text" name="quantity" value="1" min="0" max="<?php echo $produk->stok?>">
														<span class="input-group-addon product_quantity_down">-</span>
														<span class="input-group-addon product_quantity_up">+</span>
													</div>
												</div>
												<div class="cart">
													<button type="submit" class="addToCart"><i class="fa fa-shopping-cart"></i> Tambah Keranjang</button>
												</div>
											</form>
										</div>

									</div>
									<!-- end box info product -->

								</div>
							</div>
						</div>

						<section class="col-lg-2 hidden-sm hidden-md hidden-xs slider-products">
							<div class="module col-sm-12 four-block">
								<div class="modcontent clearfix">
									<div class="policy-detail">
										<div class="banner-policy">
											<div class="policy policy1">
												<a href="#"> <span class="ico-policy">&nbsp;</span> Pembayaran Aman</a>
											</div>
											<div class="policy policy2">
												<a href="#"> <span class="ico-policy">&nbsp;</span> Pengiriman Terbaik</a>
											</div>
											<div class="policy policy3">
												<a href="#"> <span class="ico-policy">&nbsp;</span> Dukungan Khusus</a>
											</div>
											<div class="policy policy4">
												<a href="#"> <span class="ico-policy">&nbsp;</span> Pengiriman Gratis
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<!-- Product Tabs -->
					<div class="producttab ">
						<div class="tabsslider  vertical-tabs col-xs-12">
							<ul class="nav nav-tabs col-lg-2 col-sm-3">
								<li class="active"><a data-toggle="tab" href="#tab-1">Deskripsi</a></li>
							</ul>
							<div class="tab-content col-lg-10 col-sm-9 col-xs-12">
								<div id="tab-1" class="tab-pane fade active in">
									<p>
										<?php echo $produk->deskripsi?>
									</p>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

	<?php $this->load->view('front/partials/script');?>

</body>

</html>
