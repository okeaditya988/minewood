<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Kategori</title>

	<?php $this->load->view('front/partials/stylesheet');?>

</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
				<?php if($jenis_barang){?>
					<li><a href="<?php echo base_url().'Kategori?id='.$jenis_barang->id_jenis_barang;?>"><?php echo $jenis_barang->jenis_barang?></a></li>
				<?php }?>
			</ul>

			<div class="row">
				
				<div id="content" class="col-md-12 col-sm-12">
					<div class="products-category">
						
						<div class="products-list row grid">

							<?php foreach($tampil as $key){?>
								<div class="product-layout col-md-3 col-sm-6 col-xs-12 ">
									<div class="product-item-container">
										<div class="left-block">
											<div class="product-image-container lazy second_img ">
												<img data-src="<?php echo base_url().'uploads/barang/'.$key->foto_barang;?>"
													src="<?php echo base_url().'uploads/barang/'.$key->foto_barang;?>"
													alt="<?php echo $key->nama_barang?>" class="img-responsive" />
												<img data-src="<?php echo base_url().'uploads/barang/'.$key->foto_barang;?>"
													src="<?php echo base_url().'uploads/barang/'.$key->foto_barang;?>"
													alt="<?php echo $key->nama_barang?>" class="img_0 img-responsive" />
											</div>
											<a class="quickview visible-lg" href="<?php echo base_url().'Detail_Produk?id='.$key->id_barang?>"> Lihat Produk</a>
										</div>
										<div class="right-block">
											<div class="caption">
												<h4><a href="<?php echo base_url().'Detail_Produk?id='.$key->id_barang?>"><?php echo $key->nama_barang?></a></h4>
												<div class="price">
													<span class="price-new"><?php echo formatRupiah($key->harga_jual)?></span>
												</div>
											</div>
											<div class="button-group text-center">
												<a href="<?php echo base_url().'Keranjang/tambah?id='.$key->id_barang?>"><button class="addToCart" type="button" data-toggle="tooltip" title="Tambah Keranjang"><i class="fa fa-shopping-cart"></i> <span class="">Tambah Keranjang</span></button></a>
											</div>
										</div>

									</div>
								</div>
								<div class="clearfix visible-xs-block"></div>
							<?php }?>

						</div>
					</div>
				</div>

			</div>
		</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

	<?php $this->load->view('front/partials/script');?>

</body>

</html>
