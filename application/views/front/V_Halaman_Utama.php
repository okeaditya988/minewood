
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Minewood - Halaman Utama</title>

	<?php $this->load->view('front/partials/stylesheet');?>

</head>

<body class="common-home res layout-home4">
	
    <div id="wrapper" class="wrapper-full banners-effect-11">
	
		<?php $this->load->view('front/partials/header');?>

		<?php $this->load->view('front/partials/slider');?>
		
		<div class="main-container container">
			
			<div class="row">
				<div id="content" class="col-sm-12">
					
					<?php if(count($barang_terlaris)>1){?>
						<div class="module tab-slider titleLine">
							<h3 class="modtitle">Produk Terlaris</h3>
							<div id="so_listing_tabs_1" class="so-listing-tabs first-load module">
								<div class="loadeding"></div>
								<div class="ltabs-wrap">
									<div class="ltabs-tabs-container" data-delay="300" data-duration="600" data-effect="starwars" data-ajaxurl="" data-type_source="0" data-lg="4" data-md="3" data-sm="2" data-xs="1"  data-margin="30">
										<div class="ltabs-tabs-wrap"> 
										<span class="ltabs-tab-selected">Produk Terlaris </span> <span class="ltabs-tab-arrow">▼</span>
											<div class="item-sub-cat">
												<ul class="ltabs-tabs cf">
													<li class="ltabs-tab tab-sel" data-active-content=".produk-terlaris"> <span class="ltabs-tab-label">Produk Terlaris</span> </li>
												</ul>
											</div>
										</div>
									</div>
									<div class="ltabs-items-container">
										<div class="ltabs-items ltabs-items-selected produk-terlaris grid" data-total="10">
											<div class="ltabs-items-inner ltabs-slider ">

												<?php foreach($barang_terlaris as $key){?>
													<div class="ltabs-item product-layout">
														<div class="product-item-container">
															<div class="left-block">
																<div class="product-image-container second_img ">
																	<img src="<?php echo base_url().'uploads/'.$key['foto_barang'];?>"  alt="<?php echo $key['nama_barang']?>" class="img-responsive" />
																</div>
																<a class="quickview visible-lg" href="quickview.html">  Lihat Produk</a>
															</div>
															<div class="right-block">
																<div class="caption">
																	<h4><a href="product.html"><?php echo $key['nama_barang']?></a></h4>
																	<div class="price">
																		<span class="price-new">Rp. <?php echo formatRupiah($key['harga_jual'])?></span> 	 
																	</div>
																</div>
																
																<div class="button-group text-center">
																	<a href="<?php echo base_url().'Keranjang/tambah?id='.$key['id_barang']?>"><button class="addToCart" type="button" data-toggle="tooltip" title="Tambah Keranjang"><i class="fa fa-shopping-cart"></i> <span class="">Tambah Keranjang</span></button></a>
																</div>
															</div>
														</div>
													</div>
												<?php }?>
												
											</div>	
										</div>
										<div class="ltabs-items items-category-18 grid" data-total="11">
											<div class="ltabs-loading"></div>
											
										</div>
										<div class="ltabs-items  items-category-25 grid" data-total="11">
											<div class="ltabs-loading"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php }?>
					
					<div class="module tab-slider titleLine">
						<h3 class="modtitle">Produk Terbaru</h3>
						<div id="so_listing_tabs_2" class="so-listing-tabs first-load module">
							<div class="ltabs-wrap">
								<div class="ltabs-tabs-container" data-delay="300" data-duration="600" data-effect="starwars" data-ajaxurl="" data-type_source="0" data-lg="4" data-md="3" data-sm="2" data-xs="1"  data-margin="30">
									<div class="ltabs-tabs-wrap"> 
									<span class="ltabs-tab-selected">Produk Terbaru </span> <span class="ltabs-tab-arrow">▼</span>
										<div class="item-sub-cat">
											<ul class="ltabs-tabs cf">
												<li class="ltabs-tab tab-sel" data-active-content=".produk-terbaru"> <span class="ltabs-tab-label">Produk Terbaru</span> </li>
											</ul>
										</div>
									</div>
								</div>
								<div class="ltabs-items-container">
									<div class="ltabs-items ltabs-items-selected produk-terbaru grid" data-total="10">
										<div class="ltabs-items-inner ltabs-slider ">
											
											<?php foreach($barang_terbaru as $key){?>
												<div class="ltabs-item product-layout">
													<div class="product-item-container">
														<div class="left-block">
															<div class="product-image-container second_img ">
																<img src="<?php echo base_url().'uploads/barang/'.$key->foto_barang;?>"  alt="<?php echo $key->nama_barang?>" class="img-responsive" />
															</div>
															<a class="quickview visible-lg" href="<?php echo base_url().'Detail_Produk?id='.$key->id_barang?>"> Lihat Produk</a>
														</div>
														<div class="right-block">
															<div class="caption">
																<h4><a href="<?php echo base_url().'Detail_Produk?id='.$key->id_barang?>"><?php echo $key->nama_barang?></a></h4>
																<div class="price">
																	<span class="price-new">Rp. <?php echo formatRupiah($key->harga_jual)?></span> 	 
																</div>
															</div>
															
															<div class="button-group text-center">
																<a href="<?php echo base_url().'Keranjang/tambah?id='.$key->id_barang?>"><button class="addToCart" type="button" data-toggle="tooltip" title="Tambah Keranjang"><i class="fa fa-shopping-cart"></i> <span class="">Tambah Keranjang</span></button></a>
															</div>
														</div>
													</div>
												</div>
											<?php }?>
											
										</div>
									</div>
									<div class="ltabs-items items-category-18 grid" data-total="11">
										<div class="ltabs-loading"></div>
									</div>
									<div class="ltabs-items  items-category-25 grid" data-total="11">
										<div class="ltabs-loading"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
		
				</div>
			</div>
		</div>
		
		<?php $this->load->view('front/partials/footer');?>

    </div>

<?php $this->load->view('front/partials/script');?>

</body>
</html>
