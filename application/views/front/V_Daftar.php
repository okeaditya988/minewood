<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Daftar</title>

	<?php $this->load->view('front/partials/stylesheet');?>

</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
				<li><a href="#">Akun Saya</a></li>
				<li><a href="#">Daftar</a></li>
			</ul>
			
			<div class="row">
				<div id="content" class="col-sm-12">
					<h2 class="title">Daftar Akun</h2>
					<p>Jika Anda sudah memiliki akun dengan kami, silakan masuk di <a href="<?php echo base_url();?>Login">halaman masuk</a>.</p>
					<form action="<?php echo base_url();?>Daftar/simpan" method="post" enctype="multipart/form-data" class="form-horizontal account-register clearfix">
						<fieldset id="account">
							<legend>Data Personal Anda</legend>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-lastname">Nama Lengkap</label>
								<div class="col-sm-10">
									<input type="text" name="nama" value="" placeholder="Masukkan Nama Lengkap" id="input-lastname" class="form-control" required>
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-email">E-Mail</label>
								<div class="col-sm-10">
									<input type="email" name="email" value="" placeholder="Masukkan E-Mail" id="input-email" class="form-control" required>
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-telephone">Nomor Telepon</label>
								<div class="col-sm-10">
									<input type="text" name="nomor_telepon" value="" placeholder="Masukkan Nomor Telepon" id="input-telephone" class="form-control" required>
								</div>
							</div>
						</fieldset>
						<fieldset id="address">
							<legend>Alamat Anda</legend>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-company">Nama Penerima</label>
								<div class="col-sm-10">
									<input type="text" name="nama_penerima" value="" placeholder="Masukkan Nama Penerima" id="input-company" class="form-control" required>
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-telephone2">Nomor Telepon Penerima</label>
								<div class="col-sm-10">
									<input type="text" name="nomor_telepon_penerima" value="" placeholder="Masukkan Nomor Telepon Penerima" id="input-telephone2" class="form-control" required>
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-address-1">Alamat</label>
								<div class="col-sm-10">
									<input type="text" name="alamat" value="" placeholder="Masukkan Alamat" id="input-address-1" class="form-control" required>
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-province">Provinsi</label>
								<div class="col-sm-10">
									<input type="hidden" name="id_kabupaten_old" class="id_kabupaten_old">
									<select name="id_provinsi" type="text" class="form-control id_provinsi" id="input-province" required>
										<option value="">-Pilih Provinsi-</option>
										<?php foreach($tampil_provinsi as $key){?>
											<option	value="<?php echo $key->id_provinsi;?>"><?php echo $key->nama_provinsi;?></option>
										<?php }?>
									</select>
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-district">Kabupaten</label>
								<div class="col-sm-10">
									<input type="hidden" name="id_kecamatan_old" class="id_kecamatan_old">
									<select name="id_kabupaten" type="text" class="form-control id_kabupaten" id="input-district" required>
										<option value="">-Pilih Kabupaten-</option>
									</select>
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-sub-district">Kecamatan</label>
								<div class="col-sm-10">
									<select name="id_kecamatan" type="text" class="form-control id_kecamatan" id="input-sub-district" required>
										<option value="">-Pilih Kecamatan-</option>
									</select>
								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend>Password Anda</legend>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-password">Password</label>
								<div class="col-sm-10">
									<input type="password" name="password" value="" placeholder="Masukkan Password" id="input-password" class="form-control">
								</div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-confirm">Konfirmasi Password</label>
								<div class="col-sm-10">
									<input type="password" name="confirm" value="" placeholder="Masukkan Kembali Password" id="input-confirm" class="form-control">
								</div>
							</div>
						</fieldset>
						<div class="buttons">
							<div class="pull-right">
								<input type="submit" value="Daftar" class="btn btn-primary">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

	<?php $this->load->view('front/partials/script');?>

	<script>
		$(".id_provinsi").on("change", function(){
			var id_provinsi = $(this).children("option:selected").val();
			var id_kabupaten = $(".id_kabupaten_old").val();
			$(".id_kecamatan").val("");
			$(".id_kabupaten").val("");
			$.ajax({
				url : "<?php echo base_url();?>"+'Kabupaten/get_kabupaten',
				type: "POST",
				data: {
					id_provinsi:id_provinsi,
					id_kabupaten:id_kabupaten
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kabupaten").html(respond);
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		$(".id_kabupaten").on("change", function(){
			var id_kabupaten = $(".id_kabupaten").children("option:selected").val();
			if(!id_kabupaten){
				id_kabupaten = $(".id_kabupaten_old").val();
			}
			var id_kecamatan = $(".id_kecamatan_old").val();
			$.ajax({
				url : "<?php echo base_url();?>"+'Kecamatan/get_kecamatan',
				type: "POST",
				data: {
					id_kabupaten:id_kabupaten,
					id_kecamatan:id_kecamatan
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kecamatan").html(respond);
				},
				error: function() {
					console.log('Error');
				}
			});
		});
	</script>

</body>

</html>
