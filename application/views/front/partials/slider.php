<section class="so-spotlight1 ">
	<div class="container">
		<div class="row">
			<div id="yt_header_left" class="col-md-12 col-md-12">
				<div class="slider-container "> 
					<div id="so-slideshow" >
						<div class="module slideshow">
							<div class="yt-content-slider yt-content-slider--arrow1"  data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1" data-items_column1="1" data-items_column2="1"  data-items_column3="1" data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="yes">
								<div class="yt-content-slide">
									<a href="#"><img src="<?php echo base_url();?>assets/front/image/slider/4.jpg" alt="slider3" class="img-responsive"></a>
								</div>
								<div class="yt-content-slide">
									<a href="#"><img src="<?php echo base_url();?>assets/front/image/slider/2.jpg" alt="slider2" class="img-responsive"></a>
								</div>
								<div class="yt-content-slide">
									<a href="#"><img src="<?php echo base_url();?>assets/front/image/slider/3.jpg" alt="slider3" class="img-responsive"></a>
								</div>
								<div class="yt-content-slide">
									<a href="#"><img src="<?php echo base_url();?>assets/front/image/slider/1.jpg" alt="slider1" class="img-responsive"></a>
								</div>
							</div>
						</div>
						<div class="loadeding"></div>
					</div>

					
				</div>
			</div>
			<div class="banner-html col-sm-12">
				<div class="module customhtml policy-v3">
					<div class="modcontent clearfix">
						<div class="block-policy-v3">
							<div class="policy policy1 col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="policy-inner"><span class="ico-policy"></span>
									<h2>Pembayaran Aman</h2><a href="#">Mudah dan nyaman</a>
								</div>
							</div>
							<div class="policy policy2 col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="policy-inner"><span class="ico-policy"></span><a href="#"><h2>Pengiriman</h2>Dikemas dengan baik</a>
								</div>
							</div>
							<div class="policy policy3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="policy-inner"><span class="ico-policy"></span><a href="#"><h2>Dukungan Khusus</h2>Selalu membantu anda</a>
								</div>
							</div>
							<div class="policy policy4 col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="policy-inner"><span class="ico-policy"></span><a href="#"><h2>Pengiriman Gratis</h2>se-Indonesia </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  
</section>
<!-- //Block Spotlight1  -->
