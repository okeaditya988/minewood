<header id="header" class=" variantleft type_4">
<!-- Header Top -->
<div class="header-top">
	<div class="container">
		<div class="row">
			<div class="header-top-left col-md-7 col-sm-6">
				<div class="col-md-12 col-sm-12 navbar-welcome">
					Selamat Datang di Minewood Studio <?php if($this->session->userdata('level_pembeli')!=TRUE){?><a href="<?php echo base_url();?>Registrasi"><strong>Registrasi</strong></a> / <a href="<?php echo base_url();?>Login"><strong>Masuk</strong><?php }?></a>						
				</div>
			</div>
			<div class="header-top-right collapsed-block text-right col-md-5 col-sm-6 col-xs-12">
				<h5 class="tabBlockTitle visible-xs">Lainnya<a class="expander " href="#TabBlock-1"><i class="fa fa-angle-down"></i></a></h5>
				<div class="tabBlock" id="TabBlock-1">
					<ul class="top-link list-inline">
						<?php if($this->session->userdata('level_pembeli')==TRUE){?>
						<li class="account btn-group" id="my_account">
							<a href="" title="My Account" class="btn btn-xs dropdown-toggle" data-toggle="dropdown"> Akun Saya | <?php echo $this->session->userdata('nama_pembeli')?> <span class="fa fa-angle-down"></span></a>
							<ul class="dropdown-menu ">
								<li><a href="<?php echo base_url();?>Pesanan_Pembeli"><i class="fa fa-shopping-cart"></i> Pesanan Saya</a></li>
								<li><a href="<?php echo base_url();?>Profil"><i class="fa fa-pencil-square-o"></i> Ubah Profil</a></li>
								<li><a href="<?php echo base_url();?>Login/logout"><i class="fa fa-arrow-left"></i> Keluar</a></li>
							</ul>
						</li>
						<?php }else{?>
						<li class="signin"><a href="<?php echo base_url();?>Login" class="top-link-checkout" title="login"> Masuk</a></li>
						<?php }?>
					</ul>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="header-center left">
	<div class="container">
		<div class="row">
			
			<div class="navbar-logo col-md-9 col-sm-8 col-xs-12">
				<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/front/image/logo.png" title="Your Store" alt="Your Store" style="max-height: 50px;" /></a>
			</div>
			
			<div class="col-md-3 col-sm-4 text-right shopping_cart">
				
				<div id="cart" class=" btn-group btn-shopping-cart">
					<a href="<?php echo base_url();?>Keranjang" class="top_cart">
						<div class="shopcart">
							<span class="handle pull-left visible-lg"></span>
							<span class="title">Keranjang Saya</span>
						</div>
					</a>
				</div>
				
			</div>
			
		</div>

	</div>
</div>

<div class="header-bottom compact-hidden">
	<div class="container">
		<div class="row">
			<div class="sidebar-menu col-md-4 col-sm-4 col-xs-12  ">
				<div class="responsive so-megamenu ">
					<div class="so-vertical-menu no-gutter compact-hidden">
						<nav class="navbar-default">
							<div class="container-megamenu vertical">
								<div id="menuHeading">
									<div class="megamenuToogle-wrapper">
										<div class="megamenuToogle-pattern">
											<div class="container">
												<div>
													<span></span>
													<span></span>
													<span></span>
												</div>
												Kategori Produk							
												<i class="fa pull-right arrow-circle fa-chevron-circle-up"></i>
											</div>
										</div>
									</div>
								</div>
								<div class="navbar-header">
									<button type="button" id="show-verticalmenu" data-toggle="collapse" class="navbar-toggle fa fa-list-alt">
									
									</button>
									Kategori Produk		
								</div>
								<div class="vertical-wrapper" >
									<span id="remove-verticalmenu" class="fa fa-times"></span>
									<div class="megamenu-pattern">
										<div class="container">
											<ul class="megamenu">
												<?php foreach($kategori as $key){?>
													<li class="item-vertical">
														<p class="close-menu"></p>
														<a href="<?php echo base_url().'Kategori?id='.$key->id_jenis_barang?>" class="clearfix">
															<span><?php echo $key->jenis_barang?></span>
														</a>
													</li>
												<?php }?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
			
			<div class="col-md-8 col-sm-8 search-pro collapsed-block">
				<h5 class="tabBlockTitle visible-xs">Cari <a class="expander " href="#sosearchpro"><i class="fa fa-angle-down"></i></a></h5>
				<div id="sosearchpro" class="col-xs-12 search-pro tabBlock">
					<form method="get" action="<?php echo base_url();?>Kategori">
						<div id="search0" class="search input-group">
							<input class="autosearch-input form-control" type="text" value="" size="50" autocomplete="off" placeholder="Cari Produk" name="nama">
							<span class="input-group-btn">
							<button type="submit" class="button-search btn btn-primary"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</form>
				</div>
			</div>
			
		</div>
	</div>

</div>

</header>
