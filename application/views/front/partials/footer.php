<footer class="footer-container type_footer1">
	
	<section class="footer-top">
		<div class="container content">
			<div class="row">
				
				<div class="col-sm-6 col-md-4 collapsed-block ">
					<div class="module clearfix">
						<h3 class="modtitle">Alamat	</h3>
						<div class="modcontent">
							<ul class="contact-address">
								<li><span class="fa fa-map-marker"></span> Minewood Studio, Jl. Masjid No.8, Kauman, Kowangan, Temanggung</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4 collapsed-block ">
					<div class="module clearfix">
						<h3 class="modtitle">Email </h3>
						<div class="modcontent">
							<ul class="contact-address">
								<li><span class="fa fa-envelope-o"></span> <a href="mailto:minewoodstudio@gmail.com"> minewoodstudio@gmail.com</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 collapsed-block ">
					<div class="module clearfix">
						<h3 class="modtitle">Nomor Telepon </h3>
						<div class="modcontent">
							<ul class="contact-address">
								<li><span class="fa fa-phone"></span> (0351) 7668010 </li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	
	<div class="footer-bottom-block ">
		<div class=" container">
			<div class="row">
				<div class="col-sm-5 copyright-text"> © 2022 Minewood Studio. All Rights Reserved. </div>
			</div>
			<div class="back-to-top" style="right: 30px;"><i class="fa fa-angle-up"></i><span> Top </span></div>
		</div>
	</div>
	
</footer>
