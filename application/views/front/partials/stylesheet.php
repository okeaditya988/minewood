<meta charset="utf-8">
<meta name="keywords" content="boostrap, responsive, html5, css3, jquery, theme, multicolor, parallax, retina, business" />
<meta name="author" content="Magentech">
<meta name="robots" content="index, follow" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" href="ico/favicon.png">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/bootstrap/css/bootstrap.min.css">
<link href="<?php echo base_url();?>assets/front/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/css/themecss/lib.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/css/themecss/so_megamenu.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/css/themecss/so-categories.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/css/themecss/so-listing-tabs.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/css/footer1.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/css/header4.css" rel="stylesheet">
<link id="color_scheme" href="<?php echo base_url();?>assets/front/css/home4.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/front/css/responsive.css" rel="stylesheet">
<link rel="stylesheet"href="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.css">

<style>
	.signin a::before {
		background-position: center -196px !important;
	}

	.signin:hover a::before {
		background-position: center -166px !important;
	}
</style>
