<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/themejs/libs.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/datetimepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/modernizr/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/themejs/application.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/themejs/homepage.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/themejs/toppanel.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/js/bootstrap-growl.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

<script>
    function notify(icon, type, title, message){
        $.growl({
            icon: icon,
            title: title+' : ',
            message: message,
            url: ''
        },{
            element: 'body',
            type: type,
            allow_dismiss: true,
            placement: {
                from: 'top',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            spacing: 10,
            z_index: 999999,
            delay: 2500,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            icon_type: 'class',
            template: '<div data-growl="container" class="alert" role="alert">' +
            '<button type="button" class="close ml-3" data-growl="dismiss">' +
            '<span aria-hidden="true">&times;</span>' +
            '<span class="sr-only">Close</span>' +
            '</button>' +
            '<span data-growl="icon" class="mr-1"></span>' +
            '<span data-growl="title"></span>' +
            '<span data-growl="message"></span>' +
            '<a href="#" data-growl="url"></a>' +
            '</div>'
        });
    };

    function angka(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 48 || charCode > 57)&&charCode>32)
            return false;
        return true;
    }
</script>

<?php if($this->session->flashdata('sukses')){?>
	<script>
        notify('fa fa-check', 'success', 'Sukses', '<?php echo $this->session->flashdata('sukses');?>');
	</script>
<?php }?>

<?php if($this->session->flashdata('gagal')){?>
	<script>
        notify('fa fa-times', 'danger', 'Gagal', '<?php echo $this->session->flashdata('gagal');?>');
	</script>
<?php }?>
