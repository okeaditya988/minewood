<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Profil</title>

	<?php $this->load->view('front/partials/stylesheet');?>

	<style>
		#example th {
			text-align: left;
		}
		#example td {
			padding: 10px;
		}
		.d-flex {
			display: flex;
		}
		.mr-2 {
			margin-right: 2rem;
		}

		#example, #example th, #example td {
			border: 1px solid black;
			border-collapse: collapse;
		}
	</style>
	<link rel="stylesheet"href="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.css">
</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
				<li><a href="#">Akun Saya</a></li>
				<li><a href="#">Profil</a></li>
			</ul>

			<div class="row">
				<div class="col-sm-12" id="content">
					<h2 class="title">Akun Saya</h2>
					<p class="lead">Halo, <strong><?php echo $this->session->userdata('nama_pembeli');?></strong> - Perbarui informasi data anda di sini.</p>
					<div class="row">
						<div class="col-sm-6">
							<form action="<?php echo base_url();?>Profil/ubah_profil" method="post">
								<fieldset id="personal-details">
									<legend>Data Pribadi</legend>
									<div class="form-group required">
										<label class="control-label" for="input-lastname">Nama Lengkap</label>
										<input type="text" name="nama" value="<?php echo $tampil->nama?>" placeholder="Masukkan Nama Lengkap" id="input-lastname" class="form-control" required>
									</div>
									<div class="form-group required">
										<label for="input-email" class="control-label">E-Mail</label>
										<input type="hidden" name="email_lama" value="<?php echo $tampil->email?>">
										<input type="email" name="email" value="<?php echo $tampil->email?>" placeholder="Masukkan E-Mail" id="input-email" class="form-control" required>
									</div>
									<div class="form-group required">
										<label for="input-telephone" class="control-label">Nomor Telepon</label>
										<input type="text" name="nomor_telepon" value="<?php echo $tampil->nomor_telepon?>" placeholder="Masukkan Nomor Telepon" id="input-telephone" class="form-control" required>
									</div>
								</fieldset>
								<div class="buttons clearfix">
									<div class="pull-right">
										<input type="submit" class="btn btn-md btn-primary" value="Simpan Data Pribadi">
									</div>
								</div>
							</form>
							<br>
						</div>
						<div class="col-sm-6">
							<form action="<?php echo base_url();?>Profil/ubah_password" method="post">
								<fieldset>
									<legend>Ubah Password</legend>
									<div class="form-group required">
										<label for="input-password" class="control-label">Password Baru</label>
										<input type="password" class="form-control" id="input-password"
											placeholder="Masukkan Password baru" value="" name="new_password">
									</div>
									<div class="form-group required">
										<label for="input-confirm" class="control-label">Ulangi Password Baru</label>
										<input type="password" class="form-control" id="input-confirm"
											placeholder="Masukkan kembali Password baru" value="" name="confirm_password">
									</div>
								</fieldset>
								<div class="buttons clearfix">
									<div class="pull-right">
										<input type="submit" class="btn btn-md btn-primary" value="Simpan Password">
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<fieldset id="address">
								<legend style="display: flex;justify-content:space-between;">
									<div>Daftar Alamat</div>
									<div>
										<button class="btn btn-warning btn-tambah" type="button">Tambah Alamat</button>
									</div>
								</legend>
							</fieldset>
							<div class="table-responsive">
								<table id="example" style="width: 100%;">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Penerima</th>
											<th>Nomor Telepon</th>
											<th>Alamat</th>
											<th>Kecamatan</th>
											<th>Kabupaten</th>
											<th>Provinsi</th>
											<th>Alamat Utama</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($tampil_alamat as $key){?>
											<tr>
												<td><?php echo $no;?></td>	
												<td><?php echo $key->nama_penerima;?></td>
												<td><?php echo $key->nomor_telepon;?></td>
												<td><?php echo $key->alamat;?></td>
												<td><?php echo $key->nama_kecamatan;?></td>
												<td><?php echo $key->nama_kabupaten;?></td>
												<td><?php echo $key->nama_provinsi;?></td>
												<td><?php if($key->default==='1'){echo 'Alamat Utama';}else{echo '-';};?></td>
												<td style="width: 10%;text-align:center;">
														<button class="btn btn-warning btn-ubah" style="margin-bottom: 5px;" value="<?php echo $key->id_alamat;?>">Ubah</button>
													<?php if($key->default!='1'){?>
														<button class="btn btn-danger btn-hapus" value="<?php echo $key->id_alamat;?>">Hapus</button>
													<?php }?>
												</td>
											</tr>
										<?php $no++; }?>
									</tbody>
								</table>
							</div>
						<div class="buttons clearfix" style="margin-top: 20px;">
							<div class="pull-right">
								<input type="submit" class="btn btn-md btn-primary" value="Simpan Password">
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

	<div class="modal fade modal-tampil" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"></h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<form class="form-horizontal form-label-left" method="POST" autocomplete="off">
					<div class="modal-body">
						<input name="id_alamat" type="hidden" class="id_alamat">
						<input name="id_pembeli" type="hidden" class="id_pembeli" value="<?php echo $tampil->id_pembeli?>">
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nama Penerima <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="nama_penerima" type="text" class="form-control nama_penerima" maxlength="30" placeholder="Masukkan Nama Penerima" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Nomor Telepon <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input name="nomor_telepon" type="text" class="form-control nomor_telepon" old="" maxlength="16" placeholder="Masukkan Nomor Telepon" required>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Alamat <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<textarea name="alamat" class="form-control alamat" old="" minlength="5" maxlength="100" placeholder="Masukkan Alamat" required></textarea>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Provinsi <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input type="hidden" name="id_kabupaten_old" class="id_kabupaten_old">
								<select name="id_provinsi" type="text" class="form-control id_provinsi" required>
									<option value="">-Pilih Provinsi-</option>
									<?php foreach($tampil_provinsi as $key){?>
										<option	value="<?php echo $key->id_provinsi;?>"><?php echo $key->nama_provinsi;?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Kabupaten <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<input type="hidden" name="id_kecamatan_old" class="id_kecamatan_old">
								<select name="id_kabupaten" type="text" class="form-control id_kabupaten" required>
									<option value="">-Pilih Kabupaten-</option>
								</select>
							</div>
						</div>
						<div class="form-group row align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Kecamatan <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<select name="id_kecamatan" type="text" class="form-control id_kecamatan" required>
									<option value="">-Pilih Kecamatan-</option>
								</select>
							</div>
						</div>
						<div class="form-group row  align-items-center">
							<label class="control-label col-md-3 col-sm-3 ">Alamat Utama? <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 ">
								<select name="default" class="form-control default" required>
									<option value="">-Pilih Alamat Utama-</option>
									<option value="1">Alamat Utama</option>
									<option value="0">Bukan Alamat Utama</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-dark btn-tutup" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary btn-simpan">Simpan</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<?php $this->load->view('front/partials/script');?>
	<script src="<?php echo base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

	<script>
		$(".btn-tambah").on("click", function(){
			$("#myModalLabel").html('Tambah Data');
      		$(".form-horizontal").attr("action","<?php echo base_url();?>"+'Profil/tambah_alamat');
			clear();
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-detail", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Detail Data');
      		$(".form-horizontal").attr("action","");
			clear();
			get_data(id);
      		$("form :input:not('.btn-tutup')").prop("disabled",true);
			$(".modal-tampil").modal("show");
		});
		
		$(document).on("click",".btn-ubah", function(){
			var id = $(this).attr("value");
			$("#myModalLabel").html('Ubah Data');
      		$(".form-horizontal").attr("action","<?php echo base_url();?>"+'Profil/ubah_alamat');
			clear();
			get_data(id);
			$(".modal-tampil").modal("show");
		});

		$(document).on("click",".btn-hapus", function(){
			var id = $(this).attr("value");
			hapus(id);
		});

		$(".id_provinsi").on("change", function(){
			var id_provinsi = $(this).children("option:selected").val();
			var id_kabupaten = $(".id_kabupaten_old").val();
			$(".id_kecamatan").val("");
			$(".id_kabupaten").val("");
			$.ajax({
				url : "<?php echo base_url();?>"+'Kabupaten/get_kabupaten',
				type: "POST",
				data: {
					id_provinsi:id_provinsi,
					id_kabupaten:id_kabupaten
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kabupaten").html(respond);
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		$(".id_kabupaten").on("change", function(){
			var id_kabupaten = $(".id_kabupaten").children("option:selected").val();
			if(!id_kabupaten){
				id_kabupaten = $(".id_kabupaten_old").val();
			}
			var id_kecamatan = $(".id_kecamatan_old").val();
			$.ajax({
				url : "<?php echo base_url();?>"+'Kecamatan/get_kecamatan',
				type: "POST",
				data: {
					id_kabupaten:id_kabupaten,
					id_kecamatan:id_kecamatan
				},
				dataType: 'json',
				success: function(respond){
					$(".id_kecamatan").html(respond);
				},
				error: function() {
					console.log('Error');
				}
			});
		});

		function get_data(id_alamat){
			$.ajax({
				url : "<?php echo base_url();?>"+'Profil/tampil_alamat',
				type: "POST",
				data: {
					id_alamat:id_alamat
				},
				dataType: 'json',
				success: function(respond){
					$(".id_alamat").val(respond.id_alamat);
					$(".id_pembeli").val(respond.id_pembeli);
					$(".nama_penerima").val(respond.nama_penerima);
					$(".nomor_telepon").val(respond.nomor_telepon);
					$(".id_kecamatan_old").val(respond.id_kecamatan);
					$(".id_kabupaten_old").val(respond.id_kabupaten);
					$(".id_provinsi").val(respond.id_provinsi).change();
					$(".id_kabupaten").change();
					$(".alamat").val(respond.alamat);
					$(".default").val(respond.default);
				},
				error: function() {
					console.log('Error');
				}
			});
		};

		function clear(){
		$('.form-horizontal').trigger("reset");
		$('.form-horizontal input, .form-horizontal select, .form-horizontal textarea, .form-horizontal button').prop("disabled",false);
	}

	function hapus(id,tabel){
		Swal.fire({
			html:
				'<p class="font-weight-bold mt-3" style="font-family: Roboto, sans-serif;font-size: 16px;"><b>Apakah Anda yakin ingin menghapus Data ini?</b></p><br><p class="font-weight-regular mb-1 px-3" style="font-family: Roboto, sans-serif; font-size: 16px;color: #7C8CA3!important " text-align="justify">Data yang telah dihapus tidak akan bisa dipulihkan kembali.</p>',
			showCancelButton: true,
			cancelButtonText: "Batal",
			confirmButtonText: 'Hapus',
			confirmButtonColor: '#dc3545',
			reverseButtons: true
		}).then((result) => {
			if(result.value){
				$.ajax({
					url : "<?php echo base_url();?>Profil/hapus_alamat",
					type : "POST",
					data : {
						id : id
					},
					success : function(data) {
                        notify('fa fa-check', 'success', 'Sukses', 'Berhasil Dihapus');
						setTimeout(
							function() {
								location.reload();
							}, 1000
						);
					},
					error : function () {
                        notify('fa fa-times', 'danger', 'Gagal', 'Gagal Dihapus');
						setTimeout(
							function() {
								location.reload();
							}, 1000
						);
					}
				});
			}
		});
	}
	</script>

</body>

</html>
