<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Riwayat Pesanan</title>

	<?php $this->load->view('front/partials/stylesheet');?>

</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
				<li><a href="#">Akun Saya</a></li>
				<li><a href="#">Riwayat Pesanan</a></li>
			</ul>
			
			<div class="row">
				<div id="content" class="col-sm-12">
					<h2 class="title">Riwayat Pesanan</h2>
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td class="text-center">No</td>
									<td>Tanggal Transaksi</td>
									<td class="text-right">Total</td>
									<td class="text-center">Status</td>
									<td class="text-center">Aksi</td>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($tampil_pesanan as $key){?>
									<tr>
										<td class="text-center"><?php echo $no;?></td>
										<td><?php echo longdate_indo($key->tanggal_pesanan).' Pukul '.time_indo($key->tanggal_pesanan); ?></td>
										<td class="text-right">Rp <?php echo formatRupiah($key->total_pesanan); ?></td>
										<td class="text-center"><?php echo $key->status_pesanan; ?></td>
										<td class="text-center">
											<a class="btn btn-primary btn-sm btn-detail" href="<?php echo base_url().'Pesanan_Pembeli/detail?id='.$key->id_pesanan; ?>">Detail Pesanan</a>
										</td>
									</tr>
								<?php $no++; }?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

	<?php $this->load->view('front/partials/script');?>

</body>

</html>
