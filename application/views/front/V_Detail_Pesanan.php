<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Riwayat Pesanan</title>

	<?php $this->load->view('front/partials/stylesheet');?>

</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
				<li><a href="#">Akun Saya</a></li>
				<li><a href="<?php echo base_url();?>Pesanan_Pembeli">Riwayat Pesanan</a></li>
				<li><a href="#">Detail Pesanan</a></li>
			</ul>
			
			<div class="row">
				<!--Middle Part Start-->
				<div id="content" class="col-sm-12">
					<h2 class="title">Riwayat Pesanan</h2>

					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td colspan="2" class="text-left">Detail Pesanan</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width: 50%;" class="text-left"> <b>Order ID:</b> <?php echo $tampil->id_pesanan?>
									<br>
									<b>Tanggal Pesanan:</b> <?php echo longdate_indo($tampil->tanggal_pesanan).' Pukul '.time_indo($tampil->tanggal_pesanan);?>
									<br>
									<b>Status Pesanan:</b> <?php echo $tampil->status_pesanan?>
									<br>
									<?php if($tampil->status_pengiriman=='Dalam Pengiriman'){?>
										<button class="btn btn-primary btn-diterima" id_pesanan="<?php echo $tampil->id_pesanan?>" style="margin-top: 20px;">Pesanan Diterima</button></td>
									<?php }?>
									</td>
								<td style="width: 50%;" class="text-left"> <b>Metode Pembayaran:</b> <?php echo $tampil->tipe_pembayaran?>
									<br>
									<b>Metode Pengiriman:</b> <?php echo $tampil->kode_kurir?> <?php echo $tampil->nomor_resi?"Nomor Resi: ".$tampil->nomor_resi:""?>
									<br>
									<b>Status Pengiriman:</b> <?php echo $tampil->status_pengiriman?>
									<br>
									<?php if($tampil->status_pengiriman=='Dalam Pengiriman'){?>
										<button class="btn btn-primary btn-lacak" id_pesanan="<?php echo $tampil->id_pesanan?>" style="margin-top: 20px;">Lacak Pesanan</button></td>
									<?php }?>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td style="width: 50%; vertical-align: top;" class="text-left">Detail Pembeli</td>
								<td style="width: 50%; vertical-align: top;" class="text-left">Detail Alamat</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-left">Nama: <?php echo $tampil->nama;?>
									<br>Nomor Telepon: <?php echo $tampil->nomor_telepon_pembeli;?>
									<br>Email: <?php echo $tampil->email;?>
								<td class="text-left">Nama: <?php echo $tampil->nama_penerima;?>
									<br>Nomor Telepon: <?php echo $tampil->nomor_telepon_penerima;?>
									<br>Alamat: <?php echo $tampil->alamat.', '.$tampil->nama_kecamatan.', '.$tampil->nama_kabupaten.', '.$tampil->nama_provinsi;?>
							</tr>
						</tbody>
					</table>
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td class="text-left">Nama Produk</td>
									<td class="text-right">Jumlah</td>
									<td class="text-right">Harga</td>
									<td class="text-right">Sub Total</td>
								</tr>
							</thead>
							<tbody>
								<?php foreach($tampil_detail as $key){?>
									<tr>
										<td class="text-left"><?php echo $key->nama_barang?></td>
										<td class="text-right"><?php echo $key->jumlah?></td>
										<td class="text-right">Rp. <?php echo formatRupiah($key->harga_jual)?></td>
										<td class="text-right">Rp. <?php echo formatRupiah($key->sub_total)?></td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<tfoot>
								<tr>
									<td class="text-right" colspan="3"><b>Total Pesanan</b>
									</td>
									<td class="text-right">Rp. <?php echo formatRupiah($tampil->total_harga);?></td>
								</tr>
								<tr>
									<td class="text-right" colspan="3"><b>Biaya Pengiriman</b>
									</td>
									<td class="text-right">Rp. <?php echo formatRupiah($tampil->total_ongkir).' ('.($tampil->total_berat/1000).' kg)';?></td>
								</tr>
								<tr>
									<td class="text-right" colspan="3"><b>Total</b>
									</td>
									<td class="text-right">Rp. <?php echo formatRupiah($tampil->total_pesanan);?></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

	<?php $this->load->view('front/partials/script');?>

	<div class="modal fade modal-sukses" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Lacak Pesanan</h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" style="font-weight: bold;">
					<table class="table mb-5">
						<thead>
							<tr>
								<th>Waktu Pengiriman</th>
								<th>Asal</th>
								<th>Pengirim</th>
								<th>Tujuan</th>
								<th>Penerima</th>
								<th>Status Penerimaan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="waktu_penerimaan"></td>
								<td class="asal"></td>
								<td class="pengirim"></td>
								<td class="tujuan"></td>
								<td class="penerima"></td>
								<td class="status_penerimaan"></td>
							</tr>
						</tbody>
					</table>
					
					<table class="table">
						<thead>
							<tr>
								<th>Waktu</th>
								<th>Deskripsi</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody class="history-data"></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade modal-gagal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Lacak Pesanan</h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" style="font-weight: bold;">
					Data Tidak Ditemukan
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(".btn-diterima").on("click", function(){
			var id_pesanan = $(this).attr("id_pesanan");
			Swal.fire({
				html:
					'<p class="font-weight-bold mt-3" style="font-family: Roboto, sans-serif;font-size: 16px;"><b>Apakah Anda yakin telah menerima pesanan ini?</b></p><br><p class="font-weight-regular mb-1 px-3" style="font-family: Roboto, sans-serif; font-size: 16px;color: #7C8CA3!important " text-align="justify">Pesanan yang diterima tidak dapat diretur.</p>',
				showCancelButton: true,
				cancelButtonText: "Batal",
				confirmButtonText: 'Diterima',
				confirmButtonColor: '#dc3545',
				reverseButtons: true
			}).then((result) => {
				if(result.value){
					$.ajax({
						url : "<?php echo base_url();?>Pesanan_Pembeli/diterima",
						type : "POST",
						data : {
							id : id_pesanan
						},
						success : function(data) {
							notify('fa fa-check', 'success', 'Sukses', 'Berhasil Diterima');
							setTimeout(
								function() {
									location.reload();
								}, 1000
							);
						},
						error : function () {
							notify('fa fa-times', 'danger', 'Gagal', 'Gagal Diterima');
							setTimeout(
								function() {
									location.reload();
								}, 1000
							);
						}
					});
				}
			});
		});
	</script>

	<script>
		$(".btn-lacak").on("click", function(){
			var id_pesanan = $(this).attr("id_pesanan");
			$.ajax({
				url : "<?php echo base_url();?>"+'Keranjang/lacak',
				type: "POST",
				data: {
					id_pesanan:id_pesanan
				},
				dataType: 'json',
				success: function(respond){
					if(respond.status.code!=200){
						$(".modal-gagal").modal("show");
					}else{
						var detail = respond.result.details;
						$(".waktu_penerimaan").html(detail.waybill_date+" "+detail.waybill_time);
						$(".asal").html(detail.origin);
						$(".pengirim").html(detail.shippper_name);
						$(".tujuan").html(detail.destination);
						$(".penerima").html(detail.receiver_name);
						if(respond.result.delivered==true){
							var delivery = respond.result.delivery_status;
							$(".penerima").html("Diterima oleh"+delivery.pod_receiver+" pada "+delivery.pod_date+" "+delivery.pod_time);
						}else{
							$(".status_penerimaan").html(respond.result.summary.status);
						}
						var detail = '';
						$.each(respond.result.manifest, function (i, obj) {
							detail += '<tr>';
							detail += '<td>'+obj.manifest_date+" "+obj.manifest_time+'</td>';
							detail += '<td>'+obj.manifest_description+'</td>';
							detail += '<td>'+obj.manifest_code+'</td>';
							detail += '</tr>';
						});
						$(".history-data").html(detail);
						$(".modal-sukses").modal("show");
					}
				},
				error: function() {
					console.log('Error');
				}
			});
		});
	</script>
</body>

</html>
