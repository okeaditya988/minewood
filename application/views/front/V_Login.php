<!DOCTYPE html>
<html lang="en">

<head>

	<title>Minewood - Masuk</title>

	<?php $this->load->view('front/partials/stylesheet');?>

	<style>
		.font-weight-bold {
			font-weight: bold;
		}
	</style>

</head>

<body class="common-home res layout-home4">

	<div id="wrapper" class="wrapper-full banners-effect-11">

		<?php $this->load->view('front/partials/header');?>

		<div class="main-container container">
		<ul class="breadcrumb">
			<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
			<li><a href="#">Akun Saya</a></li>
			<li><a href="#">Masuk</a></li>
		</ul>
		
		<div class="row">
			<div id="content" class="col-sm-12">
				<div class="page-login">
				
					<div class="account-border">
						<div class="row">
							<div class="col-sm-6 new-customer">
								<div class="well">
									<h2 class="font-weight-bold"><i class="fa fa-file-o" aria-hidden="true"></i> Buat Akun Baru</h2>
									<p>Dengan membuat akun, Anda dapat berbelanja lebih cepat, mengetahui status pesanan, dan melacak pesanan yang telah Anda buat sebelumnya.</p>
								</div>
								<div class="bottom-form">
									<a href="<?php echo base_url();?>Daftar" class="btn btn-default pull-right">Buat Akun</a>
								</div>
							</div>
							
							<form action="<?php echo base_url();?>Login/auth" method="post" enctype="multipart/form-data">
								<div class="col-sm-6 customer-login">
									<div class="well">
										<h2 class="font-weight-bold"><i class="fa fa-file-text-o" aria-hidden="true"></i> Masuk Akun</h2>
										<div class="form-group">
											<label class="control-label " for="input-email">E-Mail</label>
											<input type="email" name="email" value="" id="input-email" class="form-control" />
										</div>
										<div class="form-group">
											<label class="control-label " for="input-password">Password</label>
											<input type="password" name="password" value="" id="input-password" class="form-control" />
										</div>
									</div>
									<div class="bottom-form">
										<input type="submit" value="Masuk" class="btn btn-default pull-right" />
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

		<?php $this->load->view('front/partials/footer');?>

	</div>

	<?php $this->load->view('front/partials/script');?>

</body>

</html>
